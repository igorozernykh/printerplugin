package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.templateBase.template.*
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiLoopStatement
import com.intellij.openapi.util.TextRange
import org.jetbrains.likePrinter.util.psiElement.*
import com.intellij.psi.PsiElementFactory
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiBlockStatement
import org.jetbrains.likePrinter.util.base.IncorrectPsiElementException
import java.util.ArrayList

import java.util.HashMap
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent.CodeBlockSpecialTemplateInsertPlace
import com.intellij.psi.PsiCodeBlock
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

abstract public class LoopComponent<ET: PsiLoopStatement>(
        printer: Printer
): StatementComponent  <ET, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , EmptyUpdateComponent<ET, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    protected fun prepareBodyPart(
                     p: ET
            , variants: MutableMap<String, FormatSet>
            ,  context: VariantConstructionContext
    ) { preparePossibleCodeBlockPart(p.getBody(), BODY_TAG, variants, context) }

    override protected fun isTemplateSuitable(p: ET, tmplt: PsiTemplate<SmartInsertPlace>): Boolean = true
}