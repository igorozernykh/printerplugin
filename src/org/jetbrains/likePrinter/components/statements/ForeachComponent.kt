package org.jetbrains.likePrinter.components.statements

import com.intellij.psi.PsiForeachStatement
import org.jetbrains.likePrinter.templateBase.template.*
import com.intellij.openapi.project.Project
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.base.IncorrectPsiElementException
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.HashSet
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ForeachComponent(
        printer: Printer
): LoopComponent<PsiForeachStatement>(printer) {
    final val ITER_PARAM_TAG = "iteration parameter"
    final val ITER_VALUE_TAG = "iterated value"

    override public fun getTemplateFromElement(newP: PsiForeachStatement): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val text = newP.getText() ?: ""

        val iterParameterSIP = newP.getIterationParameter().toSmartInsertPlace()
        insertPlaceMap.put(ITER_PARAM_TAG, iterParameterSIP)

        val iterValueSIP = newP.getIteratedValue()?.toSmartInsertPlace()
        if (iterValueSIP == null) { return null }
        insertPlaceMap.put(ITER_VALUE_TAG, iterValueSIP)

        addCBtoInsertPlaceMap(newP.getBody(), BODY_TAG, insertPlaceMap, text)

        val contentRelation = getContentRelation(text, insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiForeachStatement, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        val iterParameterVariants = getIterParamVariants(p, context)
        variants.put(ITER_PARAM_TAG, iterParameterVariants)

        val iterValueVariants = getIterValueVariants(p, context)
        if (iterValueVariants.isEmpty()) { return variants }
        variants.put(ITER_VALUE_TAG, iterValueVariants)

        prepareBodyPart(p, variants, context)

        return variants
    }

    protected fun getIterParamVariants(p: PsiForeachStatement, context: VariantConstructionContext): FormatSet {
        val iterParameter = p.getIterationParameter()
        return printer.getVariants(iterParameter, context)
    }

    protected fun getIterValueVariants(p: PsiForeachStatement, context: VariantConstructionContext): FormatSet {
        val iterValue = p.getIteratedValue()
        if (iterValue == null) { return printer.getEmptySet() }
        return printer.getVariants(iterValue, context)
    }

    override protected fun getTags(p: PsiForeachStatement): Set<String> {
        val set = HashSet<String>()
        if (hasElement(p.getIteratedValue     ())) { set.add(ITER_VALUE_TAG) }
        if (hasElement(p.getIterationParameter())) { set.add(ITER_PARAM_TAG) }
        addPossibleCodeBlockTag(set, p.getBody(), BODY_TAG)
        return set
    }
}