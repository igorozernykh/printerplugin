package org.jetbrains.likePrinter.components.statements

import com.intellij.psi.PsiSwitchLabelStatement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import java.util.HashSet
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class SwitchLabelComponent(
        printer: Printer
): StatementComponent  <PsiSwitchLabelStatement, SmartInsertPlace, PsiTemplateGen<PsiSwitchLabelStatement, SmartInsertPlace>>(printer)
 , ValueOwner          <PsiSwitchLabelStatement, PsiTemplateGen<PsiSwitchLabelStatement, SmartInsertPlace>>
 , EmptyUpdateComponent<PsiSwitchLabelStatement, SmartInsertPlace, PsiTemplateGen<PsiSwitchLabelStatement, SmartInsertPlace>> {

    override public fun getTemplateFromElement(
            newP: PsiSwitchLabelStatement
    ): PsiTemplateGen<PsiSwitchLabelStatement, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset()
        addValueToInsertPlaceMap(newP, insertPlaceMap, negShift)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
                    p: PsiSwitchLabelStatement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        prepareValueVariants(p, variants, context)
        return variants
    }

    override protected fun getTags(p: PsiSwitchLabelStatement): Set<String> {
        if (p.getCaseValue() == null) { return HashSet() }
        return setOf(VALUE_TAG)
    }

    override protected fun isTemplateSuitable(
            p: PsiSwitchLabelStatement, tmplt: PsiTemplateGen<PsiSwitchLabelStatement, SmartInsertPlace>
    ): Boolean {
        val     pHasValue = p.getCaseValue() == null
        val tmpltHasValue = tmplt.psi.getCaseValue() == null
        return pHasValue == tmpltHasValue
    }
}