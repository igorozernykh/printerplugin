package org.jetbrains.likePrinter.components.statements

import com.intellij.psi.PsiLabeledStatement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class LabeledComponent(
        printer: Printer
): StatementComponent  <PsiLabeledStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , LabelIdentifierOwner<PsiLabeledStatement>
 , EmptyUpdateComponent<PsiLabeledStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val STATEMENT_TAG: String
        get() = "statement"

    override public fun getTemplateFromElement(
            newP: PsiLabeledStatement
    ): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addLabelToInsertPlaceMap(newP, insertPlaceMap)

        val statement = newP.getStatement()
        if (statement == null) { return null }
        val sip = statement.toSmartInsertPlace()
        insertPlaceMap.put(STATEMENT_TAG, sip)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
                    p: PsiLabeledStatement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareLabelVariants(p, variants, context)

        val statementVariants = getStatementVariants(p, context)
        if (statementVariants.isEmpty()) { return variants }
        variants.put(STATEMENT_TAG, statementVariants)

        return variants
    }

    protected fun getStatementVariants(p: PsiLabeledStatement, context: VariantConstructionContext): FormatSet {
        val statement = p.getStatement()
        if (statement == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(statement, context)
        return variants
    }

    override protected fun getTags(p: PsiLabeledStatement): Set<String> {
        val set = HashSet<String>()
        set.add(LABEL_IDENTIFIER_TAG)
        if (p.getStatement() != null) { set.add(STATEMENT_TAG) }
        return set
    }

    override protected fun isTemplateSuitable(
            p: PsiLabeledStatement, tmplt: PsiTemplate<SmartInsertPlace>
    ) = true
}