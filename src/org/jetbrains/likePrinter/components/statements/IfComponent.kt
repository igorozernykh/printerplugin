package org.jetbrains.likePrinter.components.statements

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiIfStatement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.openapi.project.Project
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.Printer

import org.jetbrains.likePrinter.util.base.IncorrectPsiElementException
import com.intellij.psi.PsiBlockStatement
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.components.ConditionOwner
import java.util.HashSet
import com.intellij.psi.PsiCodeBlock
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.components.CodeBlockComponent
import com.intellij.psi.PsiStatement
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

public class IfComponent(
        printer: Printer
): StatementComponent  <PsiIfStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ConditionOwner      <PsiIfStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiIfStatement, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val THEN_BRANCH_TAG: String
        get() = "then"
    final val ELSE_BRANCH_TAG: String
        get() = "else"

    override public fun getTemplateFromElement(newP: PsiIfStatement): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val text = newP.getText() ?: ""

        addConditionToInsertPlaceMap(newP, insertPlaceMap)
        if (!addCBtoInsertPlaceMap(newP.getThenBranch(), THEN_BRANCH_TAG, insertPlaceMap, text)) { return null }
        addCBtoInsertPlaceMap(newP.getElseBranch(), ELSE_BRANCH_TAG, insertPlaceMap, text)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
            p: PsiIfStatement, context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareConditionPart(p, variants, context)
        preparePossibleCodeBlockPart(p.getThenBranch(), THEN_BRANCH_TAG, variants, context)
        preparePossibleCodeBlockPart(p.getElseBranch(), ELSE_BRANCH_TAG, variants, context)

        return variants
    }

    override protected fun getTags(p: PsiIfStatement): Set<String> {
        val set = HashSet<String>()
        set.add(CONDITION_TAG)
        addPossibleCodeBlockTag(set, p.getThenBranch(), THEN_BRANCH_TAG)
        addPossibleCodeBlockTag(set, p.getElseBranch(), ELSE_BRANCH_TAG)
        return set
    }

    override protected fun isTemplateSuitable(p: PsiIfStatement, tmplt: PsiTemplate<SmartInsertPlace>): Boolean = true
}