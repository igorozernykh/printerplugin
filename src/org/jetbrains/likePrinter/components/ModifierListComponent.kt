package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiModifierList
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiElementFactory
import java.util.HashMap
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiKeyword
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.util.string.getFillConstant
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.HashSet
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.Format
import org.jetbrains.format.FormatSet
import org.jetbrains.likePrinter.util.base.fillListByWidth

/**
 * User: anlun
 */
public class ModifierListComponent(
        printer: Printer
): PsiElementComponent <PsiModifierList, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , AnnotationOwner     <PsiModifierList, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiModifierList, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {

    final val MODIFIER_TAG: String
        get() = "modifiers"

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiModifierList? {
        if (text.length() <= 0) { return null }
        val methodText = "$text\nvoid main() {}"
        val method = elementFactory.createMethodFromText(methodText, null)
        val newP = method.getModifierList()
        return newP
    }

    private fun PsiModifierList.getKeywords(): List<PsiElement> = getChildren().filter { e -> e is PsiKeyword }

    private fun PsiModifierList.getKeywordRange(): TextRange? {
        val keywords = getKeywords()
        if (keywords.isEmpty()) { return null }
        val initialRange = keywords[0].getTextRange()
        if (initialRange == null) { return null }
        return keywords.drop(1).fold(initialRange) { r, e ->
            val curRange = e.getTextRange()
            r.union(curRange ?: r)
        }
    }

    override public fun getTemplateFromElement(newP: PsiModifierList): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addAnnotationsToInsertPlaceMap(newP, insertPlaceMap, 0)

        val keywordRange = newP.getKeywordRange()
        if (keywordRange != null) {
            val text = newP.getContainingFile()?.getText() ?: ""
            val fillConstant = text.getFillConstant(keywordRange)
            insertPlaceMap.put(MODIFIER_TAG, SmartInsertPlace(keywordRange, fillConstant, Box.getEverywhereSuitable()))
        }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    private fun getModifierVariants(p: PsiModifierList, context: VariantConstructionContext): FormatSet {
        val keywords = p.getKeywords()
        if (keywords.isEmpty()) { return printer.getEmptySet() }
        val keywordVariants = keywords.map { e -> printer.getVariants(e, context) }
        return keywordVariants.fillListByWidth(printer, context.widthToSuit, { it }, Format.line(" "))
    }

    override protected fun prepareSubtreeVariants(
                    p: PsiModifierList
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareAnnotationVariants(p, variants, context)
        val modifierVariants = getModifierVariants(p, context)
        if (modifierVariants.isNotEmpty()) {
            variants.put(MODIFIER_TAG, modifierVariants)
        }

        return variants
    }

    override protected fun getTags(p: PsiModifierList): Set<String> {
        val set = HashSet<String>()

        val annotations = p.getAnnotations()
        if (annotations.isNotEmpty()) { set.add(ANNOTATION_TAG) }

        val keywords = p.getKeywords()
        if (keywords.isNotEmpty   ()) { set.add(  MODIFIER_TAG) }

        return set
    }

    override protected fun isTemplateSuitable(p: PsiModifierList, tmplt: PsiTemplate<SmartInsertPlace>): Boolean = true
}