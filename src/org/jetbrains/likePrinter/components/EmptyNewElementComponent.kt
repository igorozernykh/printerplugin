package org.jetbrains.likePrinter.components

import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElementFactory

/**
 * User: anlun
 */
public interface EmptyNewElementComponent<ET: PsiElement, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    override public fun getTmplt(p: ET): T? {
        return getTemplateFromElement(p)
    }

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): ET? {
        return null
    }
}