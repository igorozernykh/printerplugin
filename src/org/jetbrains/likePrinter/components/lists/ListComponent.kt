package org.jetbrains.likePrinter.components.lists

import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiMethod
import java.util.ArrayList
import com.intellij.psi.JavaPsiFacade
import org.jetbrains.likePrinter.util.string.*
import org.jetbrains.likePrinter.util.psiElement.*
import java.util.HashMap
import com.intellij.openapi.util.TextRange
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import com.intellij.psi.PsiComment
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.Format
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

public class ListTemplate<ET: PsiElement, IPT: SmartInsertPlace>(
  p: ET
, insertPlaceMap: Map<String, IPT>
, tagPlaceToLineNumberMap: Map<TagPlaceLine, Int>
, lineEquationMap        : Map<Int, LineEquation>
, val listElem2Tmplt: List<Template<SmartInsertPlace>>
): PsiTemplateGen<ET, IPT>(p, insertPlaceMap, tagPlaceToLineNumberMap, lineEquationMap)

public interface ListComponent<ET: PsiElement>: PsiElementComponent<ET, SmartInsertPlace,  ListTemplate<ET, SmartInsertPlace>> {
    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): ET? = null
    override public    fun getTemplateFromElement(newP: ET): ListTemplate<ET, SmartInsertPlace>? = null

    final val  FIRST_ELEMENT_TAG: String
        get() =  "first element"
    final val SECOND_ELEMENT_TAG: String
        get() = "second element"

    final val ELEMENTS_TAG: String
        get() = "elements"

    override public fun getTmplt(p: ET): ListTemplate<ET, SmartInsertPlace>? {
        val f: (PsiElement, List<PsiElement>) -> List<Template<SmartInsertPlace>>? = { p, list ->
            try {
                createElement2Tmplt(p, list)
            } catch (e: Exception) {
                null
            }
        }
        val newP = getNormalizedElement(p) ?: p
        return getListTmplt(newP, f)
    }

    open public fun getNormalizedElement(p: ET): ET? = null

    public fun getMethodByText(methodText: String): PsiMethod? {
        val elementFactory = JavaPsiFacade.getElementFactory(printer.getProject())
        val newMethod = elementFactory?.createMethodFromText(methodText, null)
        return newMethod as? PsiMethod
    }

    public fun getMethodTextFromListElementText(elementText: String): String
    public fun getElementFromMethod(method: PsiMethod): ET?

    public fun createElement2Tmplt(p: PsiElement, list: List<PsiElement>): List<Template<SmartInsertPlace>>

    public fun toPairTemplate(
              text: String
            , firstRange: TextRange
            , firstFillConstant: Int
            , secondRange: TextRange
            , secondFillConstant: Int
    ): Template<SmartInsertPlace> {
        val insertPlaceMap = mapOf(
                  Pair( FIRST_ELEMENT_TAG, SmartInsertPlace( firstRange,  firstFillConstant, Box.getEverywhereSuitable()))
                , Pair(SECOND_ELEMENT_TAG, SmartInsertPlace(secondRange, secondFillConstant, Box.getEverywhereSuitable()))
        )
        val contentRelation = getContentRelation(text, insertPlaceMap)

        return Template(text, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    public fun createMethodElemListPairTmpltList(
              p     : PsiElement
            , pList : List<PsiElement>
            , toList: (PsiMethod) -> List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        if (pList.size() < 1) { return ArrayList() }
        val firstListElem = pList[0]

        val initialRange = firstListElem.getNotNullTextRange()
        val parentShiftedUnionListRange = pList.fold(initialRange) { t, elem -> t.union(elem.getTextRange() ?: initialRange) }
        val unionListRange = parentShiftedUnionListRange.shiftRight(-p.getTextOffset())

        val fullText = p.getText() ?: ""
        val text     = fullText.substring(unionListRange).deleteSpaces(firstListElem.getOffsetInStartLine())

        val elemText = getMethodTextFromListElementText(text)
        val elementFactory = JavaPsiFacade.getElementFactory(printer.getProject())

        val newMethod = elementFactory?.createMethodFromText(elemText, null)
        if (newMethod !is PsiMethod) { throw Exception("Uncorrect Method!") } //TODO

        val list   = toList(newMethod)
        var result = ArrayList<Template<SmartInsertPlace>>()

        elemLoop@for (i in 0..(list.lastIndex - 1)) {
            val first  = list[i]
            val second = list[i + 1]
            val firstRangeInMethod  = first .getNotNullTextRange()
            val secondRangeInMethod = second.getNotNullTextRange()

            val text = newMethod.getText()?.substring(firstRangeInMethod.union(secondRangeInMethod)) ?: ""

            val negShift    = -firstRangeInMethod.getStartOffset()
            val firstRange  =  firstRangeInMethod.shiftRight(negShift)
            val secondRange = secondRangeInMethod.shiftRight(negShift)

            val pair = toPairTemplate(text
                    , firstRange , first .getFillConstant()
                    , secondRange, second.getFillConstant()
            )

            for (tmplt in result) {
                if (tmplt.toString() == pair.toString()) {
                    continue@elemLoop
                }
            }
            result.add(pair)
        }

        return result
    }

    /// Deletes indentation //TODO
//    public fun getNormalizedList(p: ET): ET?

    protected fun getListTmplt(
              p: ET
            , f: (PsiElement, List<PsiElement>) -> List<Template<SmartInsertPlace>>?
    ): ListTemplate<ET, SmartInsertPlace>? {
        for (ch in p.getChildren()) {
            if (ch is PsiComment) {
                return null
            }
        }

        val list = getList(p)
        if (list.isEmpty()) { return null }

        val firstElem  = list[0]
        val lastElem   = list[list.lastIndex]

        val e2t = f(p, list) ?: return null //TODO: filter repetitive templates by using predefined test data

        val firstRange = firstElem.getNotNullTextRange()
        val lastRange  = lastElem .getNotNullTextRange()

        val rangeInFile = firstRange.union(lastRange)
        val text = p.getContainingFile()?.getText() ?: ""
        val fillConstant = text.getFillConstant(rangeInFile)
        val range = rangeInFile.shiftRight(-p.getCorrectTextOffset())
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        insertPlaceMap.put(ELEMENTS_TAG, SmartInsertPlace(range, fillConstant, Box.getEverywhereSuitable()))

        val contentRelation = getContentRelation(p.getText() ?: "", insertPlaceMap)
        return ListTemplate(p, insertPlaceMap, contentRelation.first, contentRelation.second, e2t)
    }

    final val ELEMENT_VARIANTS_TAG: String
        get() = "element variants"

    protected fun getList(p: ET): List<PsiElement>

    override protected fun prepareSubtreeVariants(p: ET, context: VariantConstructionContext): Map<String, FormatSet> {
        /*
        val variants = HashMap<String, FormatSet>()
        val list     = getList(p)

        var counter = 0
        list.map { e -> variants.put(ELEMENT_VARIANTS_TAG + counter, printer.getVariants(e, context)); counter++ }

        return variants
        */
        return mapOf(Pair(ELEMENTS_TAG, getElementsVariants_new(p, context)))
    }
    override protected fun updateSubtreeVariants(
              p       : ET
            , tmplt   : ListTemplate<ET, SmartInsertPlace>
            , variants: Map<String, FormatSet>
            ,  context: VariantConstructionContext
    ): Map<String, FormatSet>  = variants /*{

        /*
        val list = getList(p)
        if (list.isEmpty()) { return mapOf(Pair(ELEMENTS_TAG, printer.getInitialSet())) }

        val elemVariantsArray = Array<FormatSet>(list.size) { i ->
            val curElemVariants = variants.get(ELEMENT_VARIANTS_TAG + i)
            curElemVariants ?: printer.getEmptySet()
        }

        val listElem2Tmplt = tmplt.listElem2Tmplt
        var elemListVariants = elemVariantsArray[elemVariantsArray.lastIndex]
        for (i in (list.lastIndex - 1) downTo 0) {
            elemListVariants = listElem2Tmplt.flatMap { t ->
                getVariants(
                          t.text
                        , t.insertPlaceMap
                        , mapOf(Pair(FIRST_ELEMENT_TAG, elemVariantsArray[i]), Pair(SECOND_ELEMENT_TAG, elemListVariants))
                ) ?: printer.getEmptySet()
            }
            //TODO: may be remove some variants from elemListVariants
            elemListVariants = elemListVariants.filter(printer.filterFmt(context.widthToSuit.getSuitWidth()))
        }

        return mapOf(Pair(ELEMENTS_TAG, elemListVariants))
        */
        return mapOf(Pair(ELEMENTS_TAG, getElementsVariantsByTemplate(p, tmplt, context)))
    }
    */

    protected fun getElementsVariants(
                    p: ET
            ,   tmplt: ListTemplate<ET, SmartInsertPlace> //TODO: May be unneeded
            , context: VariantConstructionContext
    ): FormatSet

    protected fun getElementsVariants_new(
                    p: ET
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())
    open public fun getSeparator_1(): Format = Format.line(", ")

    protected fun getElementsVariants(
                      p: ET
            ,   context: VariantConstructionContext
            , separator: Format
    ): FormatSet = getElementsVariants(getList(p), context, { _ -> separator })

    open protected fun getElementsVariantsByTemplate(
                    p: ET
            ,   tmplt: ListTemplate<ET, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet {
        return getElementsVariants(p, tmplt, context)
        /*
        //TODO: make work with context widthToSuit!!!

        val list = getList(p)
        if (list.isEmpty()) { return printer.getInitialSet() }

        val elemVariantsList = list.map { e -> printer.getVariants(e, context) }
        val elemVariantsListReversed = elemVariantsList.reverse()

        val listElem2Tmplt = tmplt.listElem2Tmplt
        var elemListVariants: FormatSet = elemVariantsListReversed.get(0)
        for (elemVariants in elemVariantsListReversed.drop(1)) {
            elemListVariants = listElem2Tmplt.flatMap { t ->
                getVariants(
                          t.text
                        , t.insertPlaceMap
                        , mapOf(Pair(FIRST_ELEMENT_TAG, elemVariants), Pair(SECOND_ELEMENT_TAG, elemListVariants))
                ) ?: printer.getEmptySet()
            }
            //TODO: may be remove some variants from elemListVariants
            elemListVariants = elemListVariants.filter(printer.filterFmt(context.widthToSuit.getSuitWidth()))
        }

        return elemListVariants
        */
    }

    override protected fun getTags(p: ET) = setOf(ELEMENTS_TAG)
}