package org.jetbrains.likePrinter.components.lists

import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import com.intellij.psi.PsiExpressionList
import java.util.ArrayList
import com.intellij.psi.PsiCall
import com.intellij.psi.PsiMethod
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ExpressionListComponent(
        printer: Printer
): PsiElementComponent<PsiExpressionList, SmartInsertPlace, ListTemplate<PsiExpressionList, SmartInsertPlace>>(printer)
 , ListComponent      <PsiExpressionList> {

    override public fun getMethodTextFromListElementText(elementText: String): String =
            "void main() {print\n($elementText)\n;}"

    override public fun getElementFromMethod(method: PsiMethod): PsiExpressionList? {
        val body = method.getBody()
        val statements = body?.getStatements()
        if (statements == null || statements.isEmpty()) { return null }
        val statement = statements.get(0) as? PsiCall
        val argumentList = statement?.getArgumentList()
        return argumentList
    }

    override public fun createElement2Tmplt(
                 p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                p, list
                , { m -> getElementFromMethod(m)?.getExpressions()?.toList() ?: ArrayList<PsiElement>() }
        )
    }

    override protected fun isTemplateSuitable(
            p: PsiExpressionList, tmplt: ListTemplate<PsiExpressionList, SmartInsertPlace>
    ): Boolean = true

    override protected fun getList(p: PsiExpressionList): List<PsiElement> = p.getExpressions().toList()

    override protected fun getElementsVariants(
                    p: PsiExpressionList
            ,   tmplt: ListTemplate<PsiExpressionList, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())
}