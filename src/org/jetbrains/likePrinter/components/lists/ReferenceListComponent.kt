package org.jetbrains.likePrinter.components.lists

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiReferenceList
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import com.intellij.psi.PsiMethod
import java.util.ArrayList
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

public class ReferenceListComponent(
        printer: Printer
): PsiElementComponent<PsiReferenceList, SmartInsertPlace, ListTemplate<PsiReferenceList, SmartInsertPlace>>(printer)
 , ListComponent      <PsiReferenceList> {

    override public fun getMethodTextFromListElementText(elementText: String): String =
            "void main() throws\n$elementText\n{}"

    override public fun getElementFromMethod(method: PsiMethod): PsiReferenceList? =
            method.getThrowsList()

    override public fun createElement2Tmplt(
              p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                  p, list
                , { m ->
                    val elementFromMethod = getElementFromMethod(m)
                    if (elementFromMethod != null) {
                        getList(elementFromMethod)
                    } else {
                        ArrayList<PsiElement>()
                    }
                }
        )
    }

    override protected fun isTemplateSuitable(
            p: PsiReferenceList, tmplt: ListTemplate<PsiReferenceList, SmartInsertPlace>
    ): Boolean {
        val tmpltPsi = tmplt.psi

        val pRole     = p.getRole()
        val tmpltRole = tmpltPsi.getRole()

        fun PsiReferenceList.Role?.isExtends(): Boolean {
            return (this == PsiReferenceList.Role.EXTENDS_BOUNDS_LIST) ||
            (this == PsiReferenceList.Role.EXTENDS_LIST)
        }
        if (pRole.isExtends() && tmpltRole.isExtends()) { return true }

        return pRole == tmpltRole
    }

    override protected fun getElementsVariants(
                    p: PsiReferenceList
            ,   tmplt: ListTemplate<PsiReferenceList, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())

    override protected fun getList(p: PsiReferenceList) = p.getReferenceElements().toList()
}