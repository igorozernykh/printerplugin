package org.jetbrains.likePrinter.components.lists

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiArrayInitializerMemberValue
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiMethod
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import java.util.ArrayList
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ArrayInitializerMemberValueComponent(
        printer: Printer
): PsiElementComponent<PsiArrayInitializerMemberValue, SmartInsertPlace, ListTemplate<PsiArrayInitializerMemberValue, SmartInsertPlace>>(printer)
 , ListComponent      <PsiArrayInitializerMemberValue> {

    override public fun getMethodTextFromListElementText(elementText: String): String =
            "@A(\n$elementText\n)void main() {}"

    override public fun getElementFromMethod(method: PsiMethod): PsiArrayInitializerMemberValue? {
        val annotations = method.getModifierList().getAnnotations()
        if (annotations.isEmpty()) { return null }
        val attributes = annotations[0].getParameterList().getAttributes()
        if (attributes.isEmpty()) { return null }
        return attributes[0].getValue() as? PsiArrayInitializerMemberValue
    }

    override public fun createElement2Tmplt(
              p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                p, list
                , { m ->
                    val elementFromMethod = getElementFromMethod(m)
                    if (elementFromMethod != null) {
                        getList(elementFromMethod)
                    } else {
                        ArrayList<PsiElement>()
                    }
                }
        )
    }

    override protected fun isTemplateSuitable(
            p: PsiArrayInitializerMemberValue, tmplt: ListTemplate<PsiArrayInitializerMemberValue, SmartInsertPlace>
    ): Boolean = true

    override protected fun getElementsVariants(
                    p: PsiArrayInitializerMemberValue
            ,   tmplt: ListTemplate<PsiArrayInitializerMemberValue, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())

    override protected fun getList(p: PsiArrayInitializerMemberValue) = p.getInitializers().toList()
}