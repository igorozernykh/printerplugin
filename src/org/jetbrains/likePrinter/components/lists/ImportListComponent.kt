package org.jetbrains.likePrinter.components.lists

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiImportList
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiMethod
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import java.util.ArrayList
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.likePrinter.components.EmptyNewElementComponent
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import com.intellij.psi.PsiImportStatementBase
import java.util.HashMap
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ImportListComponent(
    printer: Printer
): PsiElementComponent     <PsiImportList, SmartInsertPlace, Template<SmartInsertPlace>>(printer)
 , EmptyNewElementComponent<PsiImportList, SmartInsertPlace, Template<SmartInsertPlace>>
 , EmptyUpdateComponent    <PsiImportList, SmartInsertPlace, Template<SmartInsertPlace>> {

    override public fun getTemplateFromElement(newP: PsiImportList): Template<SmartInsertPlace>? = null

    override public fun getTmplt(p: PsiImportList): ListTemplate<PsiImportList, SmartInsertPlace>? = null

    override protected fun isTemplateSuitable(
                  p: PsiImportList
            , tmplt: Template<SmartInsertPlace>
    ): Boolean = true

    override protected fun prepareSubtreeVariants(
                    p: PsiImportList
            , context: VariantConstructionContext
    ): Map<String, FormatSet> = mapOf(Pair(FULL_CONSTRUCTION_TAG, getElementsVariants(p, context)))

    private fun getElementsVariants(
                    p: PsiImportList
            , context: VariantConstructionContext
    ): FormatSet {
        val list = getList(p)
        if (list.isEmpty()) { return printer.getEmptySet() }
        val importVariants = list map { i -> printer.getVariants(i, context) }
        if (importVariants.size() == 1) {
            return importVariants.get(0)
        }

        return importVariants.drop(1).fold(importVariants.get(0)) { r, e ->
            r - e
        }
    }

    override protected fun getTags(p: PsiImportList): Set<String> = setOf(FULL_CONSTRUCTION_TAG)

    protected fun getList(p: PsiImportList): List<PsiImportStatementBase> = p.getAllImportStatements().toList()
    override protected fun getTemplates(): List<Template<SmartInsertPlace>> = listOf(template)

    private val template = Template<SmartInsertPlace>(
              JUNK_TEXT, getFullConstructionInsertPlaceMap()
            , getFullConstructionTagPlaceToLineNumberMap()
            , getFullConstructionLineEquationMap()
    )
}