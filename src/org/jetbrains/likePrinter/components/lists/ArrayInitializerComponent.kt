package org.jetbrains.likePrinter.components.lists

import com.intellij.psi.PsiArrayInitializerExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.ArrayList
import com.intellij.psi.PsiAssignmentExpression
import org.jetbrains.likePrinter.util.psiElement.deleteSpaces
import com.intellij.psi.PsiMethod
import com.intellij.psi.PsiDeclarationStatement
import com.intellij.psi.PsiLocalVariable
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ArrayInitializerComponent(
        printer: Printer
): PsiElementComponent<PsiArrayInitializerExpression, SmartInsertPlace, ListTemplate<PsiArrayInitializerExpression, SmartInsertPlace>>(printer)
 , ListComponent      <PsiArrayInitializerExpression> {

    override public fun getNormalizedElement(p: PsiArrayInitializerExpression): PsiArrayInitializerExpression? {
        val normalizedText = p.deleteSpaces()
        val arrayInitializerToMethodText = { t: String ->
            "void main() {Integer a[] = \n$t\n;}"
        }
        val methodText = arrayInitializerToMethodText(normalizedText)
        val method = getMethodByText(methodText)
        if (method == null) { return null }
        val element = getElementFromMethod(method)
        return element
    }

    override public fun getMethodTextFromListElementText(elementText: String): String =
            "void main() {Integer a[] = \n{$elementText}\n;}"

    override public fun getElementFromMethod(method: PsiMethod): PsiArrayInitializerExpression? {
        val body = method.getBody()
        val statements = body?.getStatements()
        if (statements == null || statements.isEmpty()) { return null }
        val statement = statements.get(0) as? PsiDeclarationStatement
        val declaredElements = statement?.getDeclaredElements()
        if (declaredElements == null || declaredElements.isEmpty()) { return null }
        val localVariable = declaredElements.get(0) as? PsiLocalVariable
        val initializer = localVariable?.getInitializer() as? PsiArrayInitializerExpression
        return initializer
    }

    override public fun createElement2Tmplt(
              p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                p, list
                , { m -> getElementFromMethod(m)?.getInitializers()?.toList() ?: ArrayList<PsiElement>() }
        )
    }

    override protected fun isTemplateSuitable(
            p: PsiArrayInitializerExpression, tmplt: ListTemplate<PsiArrayInitializerExpression, SmartInsertPlace>
    ): Boolean = true

    override protected fun getList(p: PsiArrayInitializerExpression): List<PsiElement> = p.getInitializers().toList()
    override protected fun getElementsVariants(
                    p: PsiArrayInitializerExpression
            ,   tmplt: ListTemplate<PsiArrayInitializerExpression, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())
}