package org.jetbrains.likePrinter.components.lists

import com.intellij.psi.PsiTypeParameterList
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import com.intellij.psi.PsiMethod
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class TypeParameterListComponent(
        printer: Printer
): PsiElementComponent<PsiTypeParameterList, SmartInsertPlace, ListTemplate<PsiTypeParameterList, SmartInsertPlace>>(printer)
 , ListComponent      <PsiTypeParameterList> {

    override public fun getMethodTextFromListElementText(elementText: String): String =
            "<$elementText>\nvoid main(){}"

    override public fun getElementFromMethod(method: PsiMethod): PsiTypeParameterList? =
        method.getTypeParameterList()

    override public fun createElement2Tmplt(
              p: PsiElement
            , list: List<PsiElement>
    ): List<Template<SmartInsertPlace>> {
        return createMethodElemListPairTmpltList(
                  p, list
                , { m -> getElementFromMethod(m)?.getTypeParameters()?.toList() ?: listOf() }
        )
    }

    override protected fun isTemplateSuitable(
            p: PsiTypeParameterList, tmplt: ListTemplate<PsiTypeParameterList, SmartInsertPlace>
    ): Boolean = true

    override protected fun getElementsVariants(
                    p: PsiTypeParameterList
            ,   tmplt: ListTemplate<PsiTypeParameterList, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet = getElementsVariants(p, context, getSeparator_1())

    override protected fun getList(p: PsiTypeParameterList): List<PsiElement> = p.getTypeParameters()?.toList() ?: listOf()
}