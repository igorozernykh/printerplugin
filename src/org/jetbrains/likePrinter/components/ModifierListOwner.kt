package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiModifierListOwner
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ModifierListOwner<ET: PsiModifierListOwner, IPT: SmartInsertPlace, T: Template<IPT>>
: PsiElementComponent<ET, IPT, T> {
    final val MODIFIER_LIST_TAG: String
        get() = "modifier list"

    protected fun addModifierListToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>) {
        addModifierListToInsertPlaceMap(p, insertPlaceMap, 0)
    }

    protected fun addModifierListToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ) {
        val modifierList = p.getModifierList()
        if (modifierList != null && hasElement(modifierList)) {
            insertPlaceMap.put(MODIFIER_LIST_TAG, modifierList.toSmartInsertPlace().shiftRight(delta))
        }
    }

    protected fun prepareModifierListVariants(
            p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext
    ) {
        val modifierListVariants = getModifierListVariants(p, context)
        if (modifierListVariants.isEmpty()) { return }
        variants.put(MODIFIER_LIST_TAG, modifierListVariants)
    }

    protected fun getModifierListVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val modifierList = p.getModifierList()
        if ((modifierList == null) || !hasElement(modifierList)) { return printer.getEmptySet() }
        val variants = printer.getVariants(modifierList, context)
        return variants
    }

    override protected fun isTemplateSuitable(p: ET, tmplt: T): Boolean = isTemplateSuitable_ModifierListOwner(p, tmplt)

    protected fun isTemplateSuitable_ModifierListOwner(p: ET, tmplt: T): Boolean {
        return hasElement(p.getModifierList()) == (tmplt.insertPlaceMap.get(MODIFIER_LIST_TAG) != null)
    }

}