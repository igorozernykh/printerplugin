package org.jetbrains.likePrinter.components.classes

import com.intellij.psi.PsiClass
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiWhiteSpace
import org.jetbrains.likePrinter.util.psiElement.*
import org.jetbrains.likePrinter.util.base.InsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.psi.PsiEnumConstant

import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.Format
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ClassBodyOwner<ET: PsiClass, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val CLASS_BODY_TAG: String
        get() = "class body"

    override protected fun updateSubtreeVariants(
              p       : ET
            , tmplt   : T
            , variants: Map<String, FormatSet>
            , context : VariantConstructionContext
    ): Map<String, FormatSet> = variants /*{
        val newVariants = HashMap<String, FormatSet>()
        variants.toMap(newVariants)

        val bodyVariants = getBodyVariants(p, /*tmplt,*/ context)
        newVariants.put(CLASS_BODY_TAG, bodyVariants)

        return newVariants
    }
    */

    protected fun prepareBodyVariants(
              p       : ET
            , variants: MutableMap<String, FormatSet>
            , context : VariantConstructionContext
    ) {
        val bodyVariants = getBodyVariants(p, /*tmplt,*/ context)
        variants.put(CLASS_BODY_TAG, bodyVariants)
    }

    protected fun addClassBodyToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
    ) {
        val bodyRange = p.getBodyRange()
        if (bodyRange == null) { return }
        insertPlaceMap.put(CLASS_BODY_TAG,
                SmartInsertPlace(bodyRange, InsertPlace.STARTS_WITH_NEW_LINE, Box.getEverywhereSuitable())
        )
    }

    protected fun PsiClass.getBodyRange(): TextRange? {
        val lBraceOffset = getLBrace()?.getTextOffset() ?: 0
        val rBraceOffset = getRBrace()?.getTextOffset() ?: 0
        if (lBraceOffset + 1 >= rBraceOffset - 1 || rBraceOffset < 1) { return null }
        val braceRange = TextRange(lBraceOffset + 1, rBraceOffset - 1)

        val betweenBracesChildren = getChildren().filter { ch ->
            val range = ch.getNotNullTextRange()
            braceRange.contains(range)
        }
        val nonWhiteSpaceChildren = betweenBracesChildren.filterNot { ch -> ch is PsiWhiteSpace }
        val nonWhiteSpaceRange = nonWhiteSpaceChildren.getTextRange()

        val negOffset = -getCorrectTextOffset()
        return nonWhiteSpaceRange?.shiftRight(negOffset)
    }

    protected fun getBodyVariants(p: ET, /*tmplt: T,*/ context: VariantConstructionContext): FormatSet {
        //TODO: check if constraint differs for first and last lines

        val methodVariants      = getMethodVariants     (p, context)
        val fieldVariants       = getFieldVariants      (p, context)
        val innerClassVariants  = getInnerClassVariants (p, context)
        val initializerVariants = getInitializerVariants(p, context)
        val bodyVariants = initializerVariants % fieldVariants % methodVariants % innerClassVariants
        return bodyVariants
    }

    private fun getMethodVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val methods        = p.getMethods()
        val methodVariants = methods.map { m -> printer.getVariants(m, context) }

        //TODO: use tmplt for creating result
        val result = methodVariants.fold(printer.getInitialSet()) { r, m -> r % m }
        return result
    }

    private fun getFieldVariants(p: ET, context: VariantConstructionContext): FormatSet {
        /** ENUM PART **/
        val fields = p.getFields()
        val fieldsGroupByType = fields.groupBy { f -> f is PsiEnumConstant }
        val enumFields = fieldsGroupByType.get(true)?.sortByOffset()
        //TODO: use enum2template
        var result =
                if ((enumFields != null) && (enumFields.isNotEmpty())) {
                    val enumFieldVariants = enumFields.map { f -> printer.getVariants(f, context) }
                    val firstEnumVariants = enumFieldVariants[0]
                    val tailEnumVariants  = enumFieldVariants.drop(1)
                    val list = tailEnumVariants.fold(firstEnumVariants) { r, f -> r + Format.line(", ") + f }
                    list + Format.line(";")
                } else {
                    printer.getInitialSet()
                }

        val simpleFields = fieldsGroupByType.get(false)?.sortByOffset()
        if (simpleFields != null) {
            //TODO: use tmplt for creating result
            val simpleFieldVariants = simpleFields.map { f -> printer.getVariants(f, context) }
            val simpleFieldBlock = simpleFieldVariants.fold(printer.getInitialSet()) { r, f -> r - f }
            result = result % simpleFieldBlock
        }

        return result
    }

    private fun getInnerClassVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val innerClasses = p.getInnerClasses()
        val innerClassVariants = innerClasses.map { ic -> printer.getVariants(ic, context) }

        //TODO: use tmplt for creating result
        val result = innerClassVariants.fold(printer.getInitialSet()) { r, ic -> r % ic }
        return result
    }

    private fun getInitializerVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val initializers = p.getInitializers()
        val initializerVariants = initializers.map { i -> printer.getVariants(i, context) }

        //TODO: use tmplt for creating result
        val result = initializerVariants.fold(printer.getInitialSet()) { r, i -> r % i }
        return result
    }
}