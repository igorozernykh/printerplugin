package org.jetbrains.likePrinter.components.classes

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.format.Format
import com.intellij.psi.PsiAnonymousClass
import com.intellij.psi.PsiEnumConstant
import com.intellij.psi.PsiExpressionList
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import com.intellij.psi.PsiMethodCallExpression
import com.intellij.psi.PsiCall
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ArgumentListOwner<ET: PsiElement, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val ARGUMENT_LIST_TAG: String
        get() = "argument list"

    protected fun addArgumentListToInsertPlaceMap(
            p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>, delta: Int = 0
    ) {
        val list = getArgumentList(p)
        if (list != null && hasElement(list)) {
            insertPlaceMap.put(ARGUMENT_LIST_TAG, list.toSmartInsertPlace().shiftRight(delta))
        }
    }

    protected fun prepareArgumentListVariants(p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext) {
        val listVariants = getArgumentListVariants(p, context)
        if (listVariants.isEmpty()) { return }
        variants.put(ARGUMENT_LIST_TAG, listVariants)
    }

    protected fun getArgumentListVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val list = getArgumentList(p)
        if (list == null || !hasElement(list)) { return printer.getEmptySet() }
        val variants = printer.getVariants(list, context)
        return variants
    }

    private fun getArgumentList(p: PsiElement): PsiExpressionList? =
        when(p) {
            is PsiAnonymousClass -> p.getArgumentList()
            is PsiEnumConstant   -> p.getArgumentList()
            is PsiCall           -> p.getArgumentList()
            else -> null
        }

    protected fun isTemplateSuitable_ArgumentListOwner(p: ET, tmplt: T): Boolean {
        val argumentList = getArgumentList(p)
        return hasElement(argumentList) == (tmplt.insertPlaceMap.get(ARGUMENT_LIST_TAG) != null)
    }

    override protected fun isTemplateSuitable(p: ET, tmplt: T): Boolean = isTemplateSuitable_ArgumentListOwner(p, tmplt)
}