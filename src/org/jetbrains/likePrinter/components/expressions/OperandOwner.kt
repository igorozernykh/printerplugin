package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiExpression
import com.intellij.psi.PsiPostfixExpression
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import com.intellij.psi.PsiPrefixExpression
import com.intellij.psi.PsiTypeCastExpression
import com.intellij.psi.PsiInstanceOfExpression

import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface OperandOwner<ET: PsiExpression, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val OPERAND_TAG: String
        get() = "operand"

    protected fun addOperandToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addOperandToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addOperandToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val operandSIP = getOperand(p)?.toSmartInsertPlace()
        if (operandSIP == null) { return false }
        insertPlaceMap.put(OPERAND_TAG,
                SmartInsertPlace(
                          operandSIP.range.shiftRight(delta)
                        , operandSIP.fillConstant
                        , Box.getEverywhereSuitable()
                )
        )
        return true
    }

    protected fun prepareOperandVariants(
            p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext
    ) {
        val operandVariants = getOperandVariants(p, context)
        if (operandVariants.isEmpty()) { return }
        variants.put(OPERAND_TAG, operandVariants)
    }

    protected fun getOperandVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val operand = getOperand(p)
        if (operand == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(operand, context)
        return variants
    }


    private fun getOperand(p: PsiElement): PsiElement? =
        when(p) {
            is     PsiPrefixExpression -> p.getOperand()
            is    PsiPostfixExpression -> p.getOperand()
            is   PsiTypeCastExpression -> p.getOperand()
            is PsiInstanceOfExpression -> p.getOperand()
            else -> null
        }
}