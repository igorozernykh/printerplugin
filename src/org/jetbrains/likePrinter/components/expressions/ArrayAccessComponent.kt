package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiArrayAccessExpression
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ArrayAccessComponent(
        printer: Printer
): PsiElementComponent <PsiArrayAccessExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , ExpressionComponent <PsiArrayAccessExpression, PsiTemplate<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiArrayAccessExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>> {
    final val ARRAY_EXPRESSION_TAG: String
        get() = "array expression"
    final val INDEX_EXPRESSION_TAG: String
        get() = "index expression"

    override public fun getTemplateFromElement(newP: PsiArrayAccessExpression): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val arrayExpression = newP.getArrayExpression()
        insertPlaceMap.put(ARRAY_EXPRESSION_TAG, arrayExpression.toSmartInsertPlace())

        val indexExpression = newP.getIndexExpression()
        if (indexExpression == null) { return null }
        insertPlaceMap.put(INDEX_EXPRESSION_TAG, indexExpression.toSmartInsertPlace())

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiArrayAccessExpression
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        val arrayVariants = getArrayExpressionVariants(p, context)
        variants.put(ARRAY_EXPRESSION_TAG, arrayVariants)

        val indexVariants = getIndexExpressionVariants(p, context)
        if (indexVariants.isEmpty()) { return variants }
        variants.put(INDEX_EXPRESSION_TAG, indexVariants)

        return variants
    }

    protected fun getArrayExpressionVariants(
            p: PsiArrayAccessExpression, context: VariantConstructionContext
    ): FormatSet {
        val arrayExpression = p.getArrayExpression()
        return printer.getVariants(arrayExpression, context)
    }

    protected fun getIndexExpressionVariants(
            p: PsiArrayAccessExpression, context: VariantConstructionContext
    ): FormatSet {
        val indexExpression = p.getIndexExpression()
        if (indexExpression == null) { return printer.getEmptySet() }
        return printer.getVariants(indexExpression, context)
    }

    override protected fun getTags(p: PsiArrayAccessExpression) = setOf(ARRAY_EXPRESSION_TAG, INDEX_EXPRESSION_TAG)
    override protected fun isTemplateSuitable(p: PsiArrayAccessExpression, tmplt: PsiTemplate<SmartInsertPlace>) = true
}