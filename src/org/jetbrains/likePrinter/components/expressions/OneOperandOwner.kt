package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiExpression
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface OneOperandOwner<ET: PsiExpression>
: PsiElementComponent <ET, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
, ExpressionComponent <ET, PsiTemplate<SmartInsertPlace>>
, OperandOwner        <ET, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
, OperationSignOwner  <ET, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
, EmptyUpdateComponent<ET, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>
{
    override public fun getTemplateFromElement(newP: ET): PsiTemplate<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        addOperandToInsertPlaceMap      (newP, insertPlaceMap)
        addOperationSignToInsertPlaceMap(newP, insertPlaceMap)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplate(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(p: ET, context: VariantConstructionContext): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareOperandVariants      (p, variants, context)
        prepareOperationSignVariants(p, variants, context)

        return variants
    }

    override protected fun getTags(p: ET) = setOf(OPERAND_TAG, OPERATION_SIGN_TAG)
    override protected fun isTemplateSuitable(p: ET, tmplt: PsiTemplate<SmartInsertPlace>) = true
}