package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.psi.PsiElementFactory
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import org.jetbrains.likePrinter.templateBase.template.Template

/**
 * User: anlun
 */
public interface ExpressionComponent<ET: PsiExpression, T: Template<SmartInsertPlace>>
: PsiElementComponent<ET, SmartInsertPlace, T> {
    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): ET? {
        try {
            val newP = elementFactory.createExpressionFromText(text, null)
            return newP as? ET
        } catch(e: ClassCastException) {
            return null
        }
    }
}