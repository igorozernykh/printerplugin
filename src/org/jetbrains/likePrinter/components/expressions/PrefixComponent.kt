package org.jetbrains.likePrinter.components.expressions

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiPostfixExpression
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import com.intellij.psi.PsiPrefixExpression

/**
 * User: anlun
 */
public class PrefixComponent(
        printer: Printer
): PsiElementComponent<PsiPrefixExpression, SmartInsertPlace, PsiTemplate<SmartInsertPlace>>(printer)
 , OneOperandOwner    <PsiPrefixExpression> {
}