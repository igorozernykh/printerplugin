package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiExpression
import com.intellij.psi.PsiDoWhileStatement
import com.intellij.psi.PsiWhileStatement
import com.intellij.psi.PsiForStatement
import com.intellij.psi.PsiIfStatement
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.util.base.IncorrectPsiElementException
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import com.intellij.psi.PsiConditionalExpression
import com.intellij.psi.PsiAssertStatement
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ConditionOwner<ET: PsiElement, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val CONDITION_TAG: String
        get() = "condition"

    protected fun addConditionToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addConditionToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addConditionToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val condition = getCondition(p)
        if (condition == null) { return false }
        insertPlaceMap.put(CONDITION_TAG, condition.toSmartInsertPlace().shiftRight(delta))
        return true
    }

    protected fun getConditionInsertPlace(p: ET): SmartInsertPlace? {
        val condition = getCondition(p)
        if (condition == null) { return null }
        return condition.toSmartInsertPlace()
    }

    protected fun prepareConditionPart(
            p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext
    ) {
        val conditionVariants = getConditionVariants(p, context)
        if (conditionVariants.isEmpty()) { return }
        variants.put(CONDITION_TAG, conditionVariants)
    }

    protected fun getConditionVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val condition = getCondition(p)
        if (condition == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(condition, context)
        return variants
    }

    private fun getCondition(p: ET): PsiExpression? =
            when (p) {
                is PsiDoWhileStatement      -> p.getCondition()
                is PsiWhileStatement        -> p.getCondition()
                is PsiForStatement          -> p.getCondition()
                is PsiIfStatement           -> p.getCondition()
                is PsiConditionalExpression -> p.getCondition()
                is PsiAssertStatement       -> p.getAssertCondition()
                else -> null
            }
}