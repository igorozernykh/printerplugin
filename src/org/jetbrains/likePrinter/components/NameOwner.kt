package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiNameIdentifierOwner
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.emptyFormatList
import com.intellij.psi.PsiElement
import com.intellij.psi.PsiNameValuePair
import com.intellij.psi.PsiJavaCodeReferenceElement
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface NameOwner<ET: PsiElement, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val NAME_TAG: String
        get() = "name identifier"

    protected fun addNameToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
        addNameToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addNameToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val nameIdentifier = getNameIdentifier(p)
        if (nameIdentifier == null) { return false }
        insertPlaceMap.put(NAME_TAG, nameIdentifier.toSmartInsertPlace().shiftRight(delta))
        return true
    }

    protected fun prepareNameVariants(p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext) {
        val nameIdentifierVariants = getNameVariants(p, context)
        if (nameIdentifierVariants.isEmpty()) { return }
        variants.put(NAME_TAG, nameIdentifierVariants)
    }

    protected fun getNameVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val nameIdentifier = getNameIdentifier(p)
        if (nameIdentifier == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(nameIdentifier, context)
        return variants
    }

    private fun getNameIdentifier(p: ET): PsiElement? =
       when(p) {
           is PsiNameIdentifierOwner      -> p.getNameIdentifier()
           is PsiNameValuePair            -> p.getNameIdentifier()
           is PsiJavaCodeReferenceElement -> p.getReferenceNameElement()
           else -> null
       }
}