package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiMethod
import com.intellij.psi.PsiCodeBlock
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElementFactory
import com.intellij.psi.PsiBlockStatement
import org.jetbrains.likePrinter.util.psiElement.*
import org.jetbrains.likePrinter.util.string.*
import com.intellij.openapi.util.TextRange
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.openapi.project.Project
import java.util.HashMap
import com.intellij.psi.PsiSwitchLabelStatement
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiStatement
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.util.base.InsertPlace
import com.intellij.psi.JavaPsiFacade
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import java.util.HashSet
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.likePrinter.util.box.toBox
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

public class CodeBlockComponent(
    printer: Printer
): PsiElementComponent<PsiCodeBlock, SmartInsertPlace, PsiTemplateGen<PsiCodeBlock, SmartInsertPlace>>(printer) {
    final val STATEMENTS_TAG = "statements"
    final val      LABEL_TAG = "label"
    final val  STATEMENT_TAG = "statement"

    companion object {
        public fun getStatementsWithoutLabelsVariants(
                  printer: Printer
                ,       p: PsiCodeBlock
                , context: VariantConstructionContext
        ): FormatSet {
            val statements = p.getStatements()
            val stmVariants = statements map { stm -> printer.getVariants(stm, context) }
            return stmVariants.fold(printer.getInitialSet()) { fmtList, stmVar -> fmtList - stmVar }
        }
    }

    private class BlockWithLabelsTemplate(
      psi                         : PsiCodeBlock
    , insertPlaceMap              : Map<String, SmartInsertPlace>
    , tagPlaceToLineNumberMap     : Map<TagPlaceLine, Int>
    , lineEquationMap             : Map<Int, LineEquation>
    , val labelToStatementTemplate: Template<SmartInsertPlace>
    //TODO: may be add info about multiply cases and multiply statements
    ): PsiTemplateGen<PsiCodeBlock, SmartInsertPlace>(psi, insertPlaceMap, tagPlaceToLineNumberMap, lineEquationMap) {
        override public fun toString(): String {
            return super.toString() + labelToStatementTemplate.toString()
        }
    }

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiCodeBlock? {
        val blockP = elementFactory.createStatementFromText(text, null)
        if (blockP !is PsiBlockStatement) { return null }
        return blockP.getCodeBlock()
    }

    public fun getTemplateInBlockCase(p: PsiElement): PsiTemplateGen<PsiCodeBlock, SmartInsertPlace>? {
        val codeBlock = p.toCodeBlock()
        if (codeBlock == null) { return null }
        return getTmplt(codeBlock)
    }

    public fun getVariantsByTemplate(
                  p: PsiCodeBlock
          ,   tmplt: PsiTemplateGen<PsiCodeBlock, SmartInsertPlace>
          , context: VariantConstructionContext
    ): FormatSet {
        val newContext = VariantConstructionContext(printer.getCommentContext(p), context.widthToSuit)
        val variants = updateSubtreeVariants(p, tmplt, HashMap(), newContext)

        var resultVariants = getVariants(tmplt.text, tmplt.insertPlaceMap, variants) ?: printer.getEmptySet()
        resultVariants = printer.surroundVariantsByAttachedComments(p, resultVariants, newContext)
        resultVariants = printer.surroundVariantsByAttachedComments(p, resultVariants, context)
        return resultVariants
    }

    override public fun getTemplateFromElement(newP: PsiCodeBlock): PsiTemplateGen<PsiCodeBlock, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val statements = newP.getStatements()
        val text       = newP.getText() ?: ""

        val stmtSIP = statements.toSmartInsertPlace(text)
        if (stmtSIP != null) { insertPlaceMap.put(STATEMENTS_TAG, stmtSIP) }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)

        for (i in 0..(statements.lastIndex - 1)) {
            val label     = statements[i]
            val statement = statements[i + 1]
            if ( (label     is PsiSwitchLabelStatement) &&
                    statement !is PsiSwitchLabelStatement
            ) {
                val labelToStatementTemplate = getCodeBlockWithLabelsTemplate(text, label, statement)
                if (labelToStatementTemplate == null) { continue }
                return BlockWithLabelsTemplate(
                          newP, insertPlaceMap
                        , contentRelation.first, contentRelation.second
                        , labelToStatementTemplate
                )
            }
        }

        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    private fun getCodeBlockWithLabelsTemplate(
              blockText: String
            , label    : PsiSwitchLabelStatement
            , statement: PsiStatement
    ): Template<SmartInsertPlace>? {
        val labelRange     = label.getNotNullTextRange()
        val statementRange = statement.getNotNullTextRange()

        val unionRange = labelRange.union(statementRange)
        val labelToStatementText = blockText.substring(unionRange).deleteSpaces(label.getOffsetInStartLine())
        val elementFactory = JavaPsiFacade.getElementFactory(printer.getProject())
        val tmpBlock = elementFactory?.createStatementFromText("{$labelToStatementText}", null)
        if (tmpBlock !is PsiBlockStatement) { return null }

        val tmpCodeBlock = tmpBlock.getCodeBlock()
        val tmpLabel     = tmpCodeBlock.getStatements()[0]
        val tmpStm       = tmpCodeBlock.getStatements()[1]

        // shiftRight(-1) for "{" in tmpBlock text
        val newLabelRange     = tmpLabel.getTextRange()?.shiftRight(-1) ?: TextRange(0, 0)
        val newStatementRange = tmpStm  .getTextRange()?.shiftRight(-1) ?: TextRange(0, 0)

        val lblStmInsertPlaceMap = HashMap<String, SmartInsertPlace>()
        val labelPlace = SmartInsertPlace(
                  newLabelRange
                , InsertPlace.STARTS_WITH_NEW_LINE //label.getFillConstant()
                , Box.getEverywhereSuitable()
        )
        lblStmInsertPlaceMap.put(LABEL_TAG, labelPlace)
        val statementPlace = SmartInsertPlace(
                  newStatementRange
                , InsertPlace.STARTS_WITH_NEW_LINE //statement.getFillConstant()
                //TODO: may be calculate fillConstant for some statements
                , Box.getEverywhereSuitable()
        )
        lblStmInsertPlaceMap.put(STATEMENT_TAG, statementPlace)
        val contentRelation = getContentRelation(labelToStatementText, lblStmInsertPlaceMap)

        return Template<SmartInsertPlace>(
                  labelToStatementText, lblStmInsertPlaceMap
                , contentRelation.first, contentRelation.second
        )
    }

    override protected fun prepareSubtreeVariants(
                    p: PsiCodeBlock
            , context: VariantConstructionContext
    ): Map<String, FormatSet> = HashMap() /*{
        val variants = HashMap<String, FormatSet>()
        if (!p.containsSwitchLabels() && p.getStatements().isNotEmpty()) {
            val statementsVariants = getStatementsVariantsWithoutLabels(p, context)
            variants.put(STATEMENTS_TAG, statementsVariants)
        }
        return variants
    }
    */

    private fun PsiCodeBlock.containsSwitchLabels(): Boolean {
        for (stm in getStatements()) {
            if (stm is PsiSwitchLabelStatement) { return true }
        }
        return false
    }

    override protected fun updateSubtreeVariants(
              p       : PsiCodeBlock
            , tmplt   : PsiTemplateGen<PsiCodeBlock, SmartInsertPlace>
            , variants: Map<String, FormatSet>
            , context : VariantConstructionContext
    ): Map<String, FormatSet> {
//        if (!p.containsSwitchLabels()) { return variants }

        val newVariants = HashMap(variants)
        //val newVariants = HashMap<String, FormatSet>()
        //variants.toMap(newVariants)

        if (p.getStatements().isNotEmpty()) {
            val statementsVariants = getStatementsVariants(p, tmplt, context)
            newVariants.put(STATEMENTS_TAG, statementsVariants)
        }
        return newVariants
    }

    private fun getStatementsVariants(
                    p: PsiCodeBlock
            ,   tmplt: PsiTemplateGen<PsiCodeBlock, SmartInsertPlace>
            , context: VariantConstructionContext
    ): FormatSet {
        val statements = p.getStatements()
        if (tmplt is BlockWithLabelsTemplate) {
            return statementsWithLabelsToFmtList(statements, tmplt.labelToStatementTemplate, context) ?: printer.getEmptySet()
        }
        return getStatementsWithoutLabelsVariants(printer, p, context)
    }

    private fun statementsWithLabelsToFmtList(
              statements    : Array<PsiStatement>
            , lblToStmtTmplt: Template<SmartInsertPlace>
            , context       : VariantConstructionContext
    ): FormatSet? {
        if (statements.isEmpty()) { return printer.getEmptySet() }

        var headFmtList           = printer.getInitialSet()
        var currentLabelBlock     = printer.getInitialSet()
        var currentStatementBlock = printer.getInitialSet()

        var currentStmNumber = 0
        var isPreviousStmLabel = true
        var isCurrentStmLabel: Boolean
        while (currentStmNumber < statements.size()) {
            val stm = statements[currentStmNumber]
            isCurrentStmLabel = stm is PsiSwitchLabelStatement
            if (isPreviousStmLabel == false && isCurrentStmLabel == true) {
                val list = insertLabelStatementBlocksToTmplt(currentLabelBlock, currentStatementBlock, lblToStmtTmplt)
                if (list == null) { return null }
                headFmtList = headFmtList - list
                currentLabelBlock     = printer.getInitialSet()
                currentStatementBlock = printer.getInitialSet()
            }

            val variants = printer.getVariants(stm, context)
            if (isCurrentStmLabel) {
                currentLabelBlock     = currentLabelBlock - variants
            } else {
                currentStatementBlock = currentStatementBlock - variants
            }

            isPreviousStmLabel = isCurrentStmLabel
            currentStmNumber++
        }
        if (currentStatementBlock.isNotEmpty()) {
            val list = insertLabelStatementBlocksToTmplt(currentLabelBlock, currentStatementBlock, lblToStmtTmplt)
            if (list == null) { return null }
            headFmtList = headFmtList - list
        }

        return headFmtList
    }

    private fun insertLabelStatementBlocksToTmplt(
            labelFmtList    : FormatSet
          , statementFmtList: FormatSet
          , tmplt: Template<SmartInsertPlace>
    ): FormatSet? {
        val variants = HashMap<String, FormatSet>()
        variants.put(    LABEL_TAG, labelFmtList)
        variants.put(STATEMENT_TAG, statementFmtList)

        return getVariants(tmplt.text, tmplt.insertPlaceMap, variants)
    }

    override protected fun getTags(p: PsiCodeBlock): Set<String> {
        if (p.getStatements().isEmpty()) { return HashSet() }
        return setOf(STATEMENTS_TAG)
    }

    private fun PsiCodeBlock.hasLabels(): Boolean {
        for (stm in getStatements()) {
            if (stm is PsiSwitchLabelStatement) { return true }
        }
        return false
    }

    private fun PsiCodeBlock.getStatementBox(): Box {
        val statements = getStatements()
        val statementsRange = statements.getTextRange() ?: TextRange(0, 0)
        val text = getText()?.substring(statementsRange) ?: ""
        val blockBox = text.toBox(getOffsetInStartLine())

        return blockBox
    }

    override protected fun isTemplateSuitable(p: PsiCodeBlock, tmplt: PsiTemplateGen<PsiCodeBlock, SmartInsertPlace>): Boolean {
        //TODO: doesn't recalculate it!
        val doesBlockHaveLabels = p.hasLabels()
        val labelSuitness = !doesBlockHaveLabels || tmplt is BlockWithLabelsTemplate
        return labelSuitness
    }
}