package org.jetbrains.likePrinter.components.variables

import com.intellij.psi.PsiVariable
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.components.NameOwner
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.psi.PsiElementFactory
import org.jetbrains.likePrinter.components.ModifierListOwner
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.*
import org.jetbrains.format.Format
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.components.TypeOwner
import org.jetbrains.format.emptyFormatList
import java.util.HashSet
import com.intellij.lang.ASTNode
import com.intellij.psi.impl.PsiImplUtil
import com.intellij.psi.JavaTokenType
import java.util.ArrayList
import com.intellij.psi.tree.IElementType
import com.intellij.openapi.util.TextRange

import org.jetbrains.likePrinter.util.string.getFillConstant
import com.intellij.psi.PsiField
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */

fun PsiVariable.getBracketPairList(): List<Pair<ASTNode, ASTNode>> {
    val resultList = ArrayList<Pair<ASTNode, ASTNode>>()

    var element: ASTNode? = getNameIdentifier()?.getNode()

    fun getBracket(iet: IElementType): ASTNode? {
        element = PsiImplUtil.skipWhitespaceAndComments(element?.getTreeNext())
        if (element?.getElementType() != iet) { return@getBracket null }
        return@getBracket element
    }

    while (element != null) {
        val lbracket = getBracket(JavaTokenType.LBRACKET)
        if (lbracket == null) { break }
        val rbracket = getBracket(JavaTokenType.RBRACKET)
        if (rbracket == null) { break }
        resultList.add(Pair(lbracket, rbracket))
    }

    return resultList
}

fun PsiVariable.hasBrackets    (): Boolean = getBracketPairList().isNotEmpty()
fun PsiVariable.getBracketCount(): Int     = getBracketPairList().size()

fun PsiVariable.getBracketRange(): TextRange {
    return getBracketPairList().fold(TextRange(0, 0)) { i, b ->
        val  openBracketRange = b.first .getTextRange() ?: i
        val closeBracketRange = b.second.getTextRange() ?: i
        val curRange = openBracketRange.union(closeBracketRange)
        if (!i.isEmpty()) {
            i.union(curRange)
        } else {
            curRange
        }
    }
}

open public class VariableComponent(
        printer: Printer
): PsiElementComponent <PsiVariable, SmartInsertPlace, PsiTemplateGen<PsiVariable, SmartInsertPlace>>(printer)
 , ModifierListOwner   <PsiVariable, SmartInsertPlace, PsiTemplateGen<PsiVariable, SmartInsertPlace>>
 , NameOwner           <PsiVariable, SmartInsertPlace, PsiTemplateGen<PsiVariable, SmartInsertPlace>>
 , TypeOwner           <PsiVariable, PsiTemplateGen<PsiVariable, SmartInsertPlace>>
 , EmptyUpdateComponent<PsiVariable, SmartInsertPlace, PsiTemplateGen<PsiVariable, SmartInsertPlace>> {
    final val INITIALIZER_TAG: String
        get() = "initializer"

    final val ARRAY_BRACKET_TAG: String
        get() = "array brackets"

    private fun getNewFields(text: String, elementFactory: PsiElementFactory): Array<PsiField> {
        fun toFieldText(t: String): String {
            val trimmed = t.trim()
            if (trimmed.isEmpty()) { return "" }
            if (!trimmed[trimmed.length() - 1].equals(';')) { return t + "\n, x;" } //Added "\n, x;" for normal field without ';'
            return t
        }

        val classText = "class A{\n${toFieldText(text)}\n}"
        val dummyClass = elementFactory.createClassFromText(classText, null)
        val newClass = dummyClass.getAllInnerClasses()[0]

        return newClass.getFields()
    }

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiVariable? {
        val fields = getNewFields(text, elementFactory)
        if (fields.isNotEmpty()) {
            return fields[0]
        }
        /// if fields empty it means that variable from PsiDeclarationStatement and isn't the first one.

        val nextTryFields = getNewFields("Integer x, \n$text\n", elementFactory)
        if (nextTryFields.size() > 1) {
            // 1 - because we added the "Integer x"
            return nextTryFields[1]
        }

        return null
    }

    override public fun getTemplateFromElement(newP: PsiVariable): PsiTemplateGen<PsiVariable, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()

        val negShift = -newP.getCorrectTextOffset()
        addNameToInsertPlaceMap        (newP, insertPlaceMap, negShift)
        addModifierListToInsertPlaceMap(newP, insertPlaceMap, negShift)

        // Workaround of getTypeElement() @NotNull convention bug.
        try {
            val typeElement = newP.getTypeElement()
            val parent = typeElement?.getParent()
            if (hasElement(typeElement) && parent != null && parent.equals(newP)) {
                addTypeToInsertPlaceMap        (newP, insertPlaceMap, negShift)
            }
        } catch (e: IllegalStateException) {
        } catch (e: AssertionError) {
        }

        val bracketRange = newP.getBracketRange()
        if (!bracketRange.isEmpty()) {
            val text = newP.getContainingFile()?.getText() ?: ""
            val fillConstant = text.getFillConstant(bracketRange)
            val range = bracketRange.shiftRight(-newP.getCorrectTextOffset())
            insertPlaceMap.put(ARRAY_BRACKET_TAG, SmartInsertPlace(range, fillConstant, Box.getEverywhereSuitable()))
        }

        val initializer = newP.getInitializer()
        if (initializer != null && hasElement(initializer)) {
            insertPlaceMap.put(INITIALIZER_TAG,
                    SmartInsertPlace(
                              initializer.getNotNullTextRange().shiftRight(negShift)
                            , initializer.getFillConstant()
                            , Box.getEverywhereSuitable()
                    )
            )
        }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p      : PsiVariable
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

        prepareNameVariants        (p, variants, context)
        prepareModifierListVariants(p, variants, context)
        prepareTypeVariants        (p, variants, context)

        val bracketVariants = getArrayBracketVariants(p, context)
        if (!bracketVariants.isEmpty()) {
            variants.put(ARRAY_BRACKET_TAG, bracketVariants)
        }

        val initializerVariants = getInitializerVariants(p, context)
        if (initializerVariants.isNotEmpty()) { variants.put(INITIALIZER_TAG, initializerVariants) }

        return variants
    }

    override protected fun getTags(p: PsiVariable): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getModifierList())) { set.add(MODIFIER_LIST_TAG) }

        // Workaround of getTypeElement() @NotNull convention bug.
        try {
            val typeElement = p.getTypeElement()
            val parent = typeElement?.getParent()
            if (hasElement(typeElement) && parent != null && parent.equals(p)) { set.add(TYPE_TAG) }
        } catch (e: IllegalStateException) {
        } catch (e: AssertionError) {
        }

        set.add(NAME_TAG)
        if (hasElement(p.getInitializer()) ) { set.add(  INITIALIZER_TAG) }
        if (p.hasBrackets()                ) { set.add(ARRAY_BRACKET_TAG) }

        return set
    }

    private fun getInitializerVariants(
                    p: PsiVariable
            , context: VariantConstructionContext
    ): FormatSet {
        val initializer = p.getInitializer()
        if (initializer == null || !hasElement(initializer)) { return printer.getEmptySet() }
        return printer.getVariants(initializer, context)
    }

    private fun getArrayBracketVariants(
                    p: PsiVariable
            , context: VariantConstructionContext
    ): FormatSet = p.getBracketPairList().fold(printer.getEmptySet()) { i, b ->
        val arrayBrackets = Format.line("[]")
        if (i.isEmpty()) {
            printer.getInitialSet(arrayBrackets)
        } else {
            i / arrayBrackets
        }
    }

    override protected fun isTemplateSuitable(p: PsiVariable, tmplt: PsiTemplateGen<PsiVariable, SmartInsertPlace>): Boolean {
        // Checking for ending ';'
        val trimmedTmpltText = tmplt.text.trim()
        if (trimmedTmpltText.isEmpty()) { return false }
        val isTmpltLastSymbolSemicolon = trimmedTmpltText.get(trimmedTmpltText.length() - 1) == ';'

        val trimmedElementText = p.getText()?.trim() ?: ""
        if (trimmedElementText.isEmpty()) { return false }
        val isElementLastSymbolSemicolon = trimmedElementText.get(trimmedElementText.length() - 1) == ';'
        if (isTmpltLastSymbolSemicolon != isElementLastSymbolSemicolon) { return false }


        if (hasElement(p.getInitializer()) != (tmplt.insertPlaceMap.get(INITIALIZER_TAG) != null)) { return false }
        return isTemplateSuitable_ModifierListOwner(p, tmplt)
    }
}