package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiMethod
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ThrowsListOwner<ET: PsiMethod, T: Template<SmartInsertPlace>>: PsiElementComponent<ET, SmartInsertPlace, T> {
    final val THROWS_LIST_TAG: String
        get() = "throws list"

    protected fun addThrowsListToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val throwsList = p.getThrowsList()
        if (!hasElement(throwsList)) { return false }
        val throwsListSIP = throwsList.toSmartInsertPlace()
        insertPlaceMap.put(THROWS_LIST_TAG, throwsListSIP.shiftRight(delta))
        return true
    }

    protected fun getThrowsListVariants(p: PsiMethod, context: VariantConstructionContext): FormatSet {
        val throwsList = p.getThrowsList()
        if (!hasElement(throwsList)) { return printer.getEmptySet() }
        return printer.getVariants(throwsList, context)
    }
}