package org.jetbrains.likePrinter.components

import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiPackageStatement
import com.intellij.psi.PsiImportStatementBase
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ReferenceOwner<ET: PsiElement, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val REFERENCE_TAG: String
        get() = "package reference"

    protected fun addReferenceToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addReferenceToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addReferenceToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val reference = getReference(p)
        if (reference == null) { return false }
        val referenceSIP = reference.toSmartInsertPlace()
        insertPlaceMap.put(
                  REFERENCE_TAG
                , SmartInsertPlace(referenceSIP.range, referenceSIP.fillConstant, Box.getEverywhereSuitable()).shiftRight(delta)
        )
        return true
    }

    protected fun prepareReferenceVariants(
            p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext
    ) {
        val referenceVariants = getReferenceVariants(p, context)
        if (referenceVariants.isEmpty()) { return }
        variants.put(REFERENCE_TAG, referenceVariants)
    }

    protected fun getReferenceVariants(
            p: ET, context: VariantConstructionContext
    ): FormatSet {
        val reference = getReference(p)
        if (reference == null) { return printer.getEmptySet() }
        return printer.getVariants(reference, context)
    }

    private fun getReference(p: PsiElement): PsiElement? =
        when(p) {
            is PsiPackageStatement    -> p.getPackageReference()
            is PsiImportStatementBase -> p. getImportReference()
            else -> null
        }
}