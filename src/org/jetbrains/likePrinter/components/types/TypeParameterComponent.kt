package org.jetbrains.likePrinter.components.types

import com.intellij.psi.PsiTypeParameter
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.components.NameOwner
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.psi.PsiElementFactory
import com.intellij.util.IncorrectOperationException
import com.intellij.psi.JavaPsiFacade
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.getFillConstant
import org.jetbrains.likePrinter.util.psiElement.getNotNullTextRange
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.components.EmptyUpdateComponent

import java.util.HashSet
import org.jetbrains.likePrinter.components.AnnotationOwner
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class TypeParameterComponent(
        printer: Printer
): PsiElementComponent <PsiTypeParameter, SmartInsertPlace, PsiTemplateGen<PsiTypeParameter, SmartInsertPlace>>(printer)
 , NameOwner           <PsiTypeParameter, SmartInsertPlace, PsiTemplateGen<PsiTypeParameter, SmartInsertPlace>>
 , AnnotationOwner     <PsiTypeParameter, SmartInsertPlace, PsiTemplateGen<PsiTypeParameter, SmartInsertPlace>>
 , EmptyUpdateComponent<PsiTypeParameter, SmartInsertPlace, PsiTemplateGen<PsiTypeParameter, SmartInsertPlace>> {
    final val EXTENDS_LIST_TAG: String
        get() = "extends list"

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiTypeParameter? {
        try {
            return elementFactory.createTypeParameterFromText(text, null)
        } catch (e: IncorrectOperationException) {
            return null
        }
    }

    override public fun getTemplateFromElement(
            newP: PsiTypeParameter
    ): PsiTemplateGen<PsiTypeParameter, SmartInsertPlace>? {
//        if (!newP.hasNonEmptyExtendsList()) { return null }

        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        addNameToInsertPlaceMap(newP, insertPlaceMap)

        val extendsList = newP.getExtendsList()
        if (hasElement(extendsList)) {
            insertPlaceMap.put(
                      EXTENDS_LIST_TAG
                    , SmartInsertPlace(
                        extendsList.getNotNullTextRange(), extendsList.getFillConstant(), Box.getEverywhereSuitable()
                    )
            )
        }

        addAnnotationsToInsertPlaceMap(newP,insertPlaceMap)

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiTypeParameter
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

              prepareNameVariants(p, variants, context)
        prepareAnnotationVariants(p, variants, context)

        val extendsListVariants = getExtendsListVariants(p, context)
        if (extendsListVariants.isNotEmpty()) {
            variants.put(EXTENDS_LIST_TAG, extendsListVariants)
        }

        return variants
    }

    protected fun getExtendsListVariants(p: PsiTypeParameter, context: VariantConstructionContext): FormatSet {
        val extendsList = p.getExtendsList()
        if (!hasElement(extendsList)) { return printer.getEmptySet() }
        return printer.getVariants(extendsList, context)
    }

    override protected fun getTags(p: PsiTypeParameter): Set<String> {
        val set = HashSet<String>()

        if (hasElement(p.getExtendsList())) { set.add(EXTENDS_LIST_TAG) }
        set.add(NAME_TAG)

        val annotations = p.getAnnotations()
        if (annotations.isNotEmpty()) { set.add(ANNOTATION_TAG) }

        return set
    }

    private fun PsiTypeParameter.hasNonEmptyExtendsList(): Boolean {
        return getExtendsList().getReferenceElements().isNotEmpty()
    }

    override protected fun isTemplateSuitable(
            p: PsiTypeParameter, tmplt: PsiTemplateGen<PsiTypeParameter, SmartInsertPlace>
    ): Boolean = true
}