package org.jetbrains.likePrinter.components.types

import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiTypeElement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.components.EmptyUpdateComponent
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElementFactory
import org.jetbrains.likePrinter.components.AnnotationOwner
import java.util.HashMap
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import java.util.HashSet
import org.jetbrains.likePrinter.util.psiElement.hasElement
import com.intellij.psi.PsiKeyword
import org.jetbrains.likePrinter.components.ArrayDimensionOwner
import java.util.ArrayList
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.openapi.util.TextRange
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import org.jetbrains.likePrinter.util.box.Box
import org.jetbrains.format.Format
import org.jetbrains.format.FormatSet
import org.jetbrains.likePrinter.util.base.fillListByWidth

/**
 * User: anlun
 */
public class TypeElementComponent(
        printer: Printer
): PsiElementComponent <PsiTypeElement, SmartInsertPlace, Template<SmartInsertPlace>>(printer)
 , AnnotationOwner     <PsiTypeElement, SmartInsertPlace, Template<SmartInsertPlace>>
 , ArrayDimensionOwner <PsiTypeElement, SmartInsertPlace, Template<SmartInsertPlace>>
 , EmptyUpdateComponent<PsiTypeElement, SmartInsertPlace, Template<SmartInsertPlace>> {

    final val TYPE_TAG: String
        get() = "type"

    override protected fun getNewElement(text: String, elementFactory: PsiElementFactory): PsiTypeElement? {
        val method = elementFactory.createMethodFromText("void m($text a) {}", null)
        val parameters = method.getParameterList().getParameters()
        if (parameters.isEmpty()) { return null }
        return parameters.get(0).getTypeElement()
    }

    override public fun getTemplateFromElement(
            newP: PsiTypeElement
    ): Template<SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        if (newP.getText()?.contains("extends") ?: false ) { return null } //TODO: do smth suitable

        val negShift = -newP.getCorrectTextOffset()
           addAnnotationsToInsertPlaceMap(newP, insertPlaceMap, negShift)
        addArrayDimensionToInsertPlaceMap(newP, insertPlaceMap, negShift)

        val referenceElement = newP.getElementOfType()
        if (referenceElement != null) {
            insertPlaceMap.put(TYPE_TAG, referenceElement.toSmartInsertPlace().shiftRight(negShift))
        }

        val contentRelation = getContentRelation(newP.getText() ?: "", insertPlaceMap)
        return Template(newP.getText() ?: "", insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    protected fun getTypeVariants(p: PsiTypeElement, context: VariantConstructionContext): FormatSet {
        val referenceElement = p.getElementOfType()
        if (referenceElement == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(referenceElement, context)
        return variants
    }

    override protected fun prepareSubtreeVariants(
                    p: PsiTypeElement
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()

            prepareAnnotationVariants(p, variants, context)
        prepareArrayDimensionVariants(p, variants, context)
        val typeVariants = getTypeVariants(p, context)
        if (typeVariants.isNotEmpty()) { variants.put(TYPE_TAG, typeVariants) }

        return variants
    }

    override protected fun getTags(p: PsiTypeElement): Set<String> {
        val set = HashSet<String>()

        val annotations = p.getAnnotations()
        if (annotations.isNotEmpty()    ) { set.add(ANNOTATION_TAG) }

        val referenceElement = p.getElementOfType()
        if (hasElement(referenceElement)) { set.add(TYPE_TAG) }
        if (p.hasArrayDimensions()      ) { set.add(ARRAY_DIMENSION_TAG) }

        return set
    }

    private fun PsiTypeElement.hasArrayDimensions(): Boolean {
        val arrayDimensionSize = getType().getArrayDimensions()
        return arrayDimensionSize > 0
    }


    private fun PsiTypeElement.getElementOfType(): PsiElement? {
        val referenceElement = getInnermostComponentReferenceElement()
        if (referenceElement != null) { return referenceElement }

        var firstChild = getFirstChild()
        while (!(firstChild == null || firstChild is PsiKeyword)) {
            firstChild = firstChild.getFirstChild()
        }
        return firstChild
    }

    override protected fun getArrayDimensionVariants(p: PsiTypeElement, context: VariantConstructionContext): FormatSet {
        //TODO: may be use some sort of templates
        val arrayDimensions = p.getType().getArrayDimensions()
        if (arrayDimensions <= 0) { return printer.getEmptySet() }
        val arrayDimensionVariants = ArrayList<FormatSet>(arrayDimensions)
        for (i in 1..arrayDimensions) {
            arrayDimensionVariants.add(printer.getInitialSet(Format.line("[]")))
        }
        return arrayDimensionVariants.fillListByWidth(printer, context.widthToSuit, {x -> x}, Format.empty)
    }

    override protected fun isTemplateSuitable(p: PsiTypeElement, tmplt: Template<SmartInsertPlace>): Boolean {
        val pText = p.getText()
        if (pText == null) { return false }
        val isPMultiple     = pText.endsWith("...")
        val isTmpltMultiple = tmplt.text.endsWith("...")
        return isPMultiple == isTmpltMultiple
    }

    public fun getSimpleTemplate(): Template<SmartInsertPlace> {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        insertPlaceMap.put(TYPE_TAG, SmartInsertPlace(TextRange(0, 1), 0, Box.getEverywhereSuitable()))
        val text = "A"
        val contentRelation = getContentRelation(text, insertPlaceMap)
        return Template<SmartInsertPlace>(text, insertPlaceMap, contentRelation.first, contentRelation.second)
    }
}