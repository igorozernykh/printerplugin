package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiJavaFile
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import java.util.HashMap
import java.util.ArrayList
import java.util.HashSet
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class JavaFileComponent(
        printer: Printer
): PsiElementComponent     <PsiJavaFile, SmartInsertPlace, Template<SmartInsertPlace>>(printer)
 , EmptyNewElementComponent<PsiJavaFile, SmartInsertPlace, Template<SmartInsertPlace>>
 , EmptyUpdateComponent    <PsiJavaFile, SmartInsertPlace, Template<SmartInsertPlace>> {

    override public fun getVariants(p: PsiJavaFile, context: VariantConstructionContext): FormatSet {
        //TODO: make simple context.widthToSuit
        val list = ArrayList<FormatSet>()

        val packageStatementVariants = getPackageStatementVariants(p, context)
        if (packageStatementVariants != null) {
            if (packageStatementVariants.isEmpty()) { return printer.getEmptySet() }
            list.add(packageStatementVariants)
        }

        val importListVariants = getImportListVariants(p, context);
        if (importListVariants != null) {
            if (importListVariants.isEmpty()) { return printer.getEmptySet() }
            list.add(importListVariants)
        }

        val classVariants = getClassVariants(p, context)
        if (classVariants != null) {
            if (classVariants.isEmpty()) { return printer.getEmptySet() }
            list.add(classVariants)
        }

        if (list.isEmpty()) {
            return printer.getEmptySet()
        }

        if (list.size() == 1) {
            return list.get(0)
        }

        val firstElem = list.get(0)
        return list.drop(1).fold(firstElem) { r, e -> r % e }
    }

    private fun getPackageStatementVariants(p: PsiJavaFile, context: VariantConstructionContext): FormatSet? {
        val packageStatement = p.getPackageStatement()
        if (packageStatement == null) { return null }
        return printer.getVariants(packageStatement, context)
    }

    private fun getImportListVariants(p: PsiJavaFile, context: VariantConstructionContext): FormatSet? {
        val importList = p.getImportList()
        if (importList == null || importList.getAllImportStatements().isEmpty()) { return null }
        return printer.getVariants(importList, context)
    }

    private fun getClassVariants(p: PsiJavaFile, context: VariantConstructionContext): FormatSet? {
        val classes = p.getClasses()
        if (classes.isEmpty()) { return null }

        val classVariants = classes.map { c -> printer.getVariants(c, context) }
        if (classVariants.size() == 1) {
            return classVariants.get(0)
        }

        val firstClassVariants = classVariants.get(0)
        return classVariants.drop(1).fold(firstClassVariants) { r, cv -> r % cv }
    }

    override public fun getTemplateFromElement(newP: PsiJavaFile): Template<SmartInsertPlace>? = null
    override protected fun prepareSubtreeVariants(
                    p: PsiJavaFile
            , context: VariantConstructionContext
    ): Map<String, FormatSet> = HashMap()

    override protected fun getTags(p: PsiJavaFile): Set<String> = HashSet()
    override protected fun isTemplateSuitable(
            p: PsiJavaFile, tmplt: Template<SmartInsertPlace>
    ): Boolean = true

}