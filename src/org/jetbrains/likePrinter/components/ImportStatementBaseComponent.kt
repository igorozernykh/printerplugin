package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiImportStatementBase
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiElementFactory
import java.util.HashMap
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.likePrinter.templateBase.template.PsiTemplate
import com.intellij.psi.PsiImportStatement
import com.intellij.psi.PsiImportStaticStatement
import java.util.ArrayList
import org.jetbrains.likePrinter.util.psiElement.getCorrectTextOffset
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public class ImportStatementBaseComponent(
        printer: Printer
): PsiElementComponent     <PsiImportStatementBase, SmartInsertPlace, PsiTemplateGen<PsiImportStatementBase, SmartInsertPlace>>(printer)
 , ReferenceOwner          <PsiImportStatementBase, SmartInsertPlace, PsiTemplateGen<PsiImportStatementBase, SmartInsertPlace>>
 , EmptyNewElementComponent<PsiImportStatementBase, SmartInsertPlace, PsiTemplateGen<PsiImportStatementBase, SmartInsertPlace>>
 , EmptyUpdateComponent    <PsiImportStatementBase, SmartInsertPlace, PsiTemplateGen<PsiImportStatementBase, SmartInsertPlace>> {

    override public fun getTemplateFromElement(newP: PsiImportStatementBase): PsiTemplateGen<PsiImportStatementBase, SmartInsertPlace>? {
        val insertPlaceMap = HashMap<String, SmartInsertPlace>()
        val negShift = -newP.getCorrectTextOffset()
        addReferenceToInsertPlaceMap(newP, insertPlaceMap, negShift)
        val text = newP.getText() ?: ""
        val contentRelation = getContentRelation(text, insertPlaceMap)
        return PsiTemplateGen(newP, insertPlaceMap, contentRelation.first, contentRelation.second)
    }

    override protected fun prepareSubtreeVariants(
              p: PsiImportStatementBase
            , context: VariantConstructionContext
    ): Map<String, FormatSet> {
        val variants = HashMap<String, FormatSet>()
        prepareReferenceVariants(p, variants, context)
        return variants
    }

    override protected fun getTags(p: PsiImportStatementBase): Set<String> = setOf(REFERENCE_TAG)

    override protected fun isTemplateSuitable(
            p: PsiImportStatementBase, tmplt: PsiTemplateGen<PsiImportStatementBase, SmartInsertPlace>
    ): Boolean {
        val tmpltPsi = tmplt.psi

        //checks for .*
        if (p.isOnDemand() != tmpltPsi.isOnDemand()) { return false }

        if (p is PsiImportStatement       && tmpltPsi is       PsiImportStatement) { return true }
        if (p is PsiImportStaticStatement && tmpltPsi is PsiImportStaticStatement) { return true }

        return false
    }
}