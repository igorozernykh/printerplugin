package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import com.intellij.psi.PsiTypeParameterListOwner
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.Template
import org.jetbrains.likePrinter.util.psiElement.hasElement
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface TypeParameterListOwner<ET: PsiTypeParameterListOwner, IPT: SmartInsertPlace, T: Template<IPT>>
: PsiElementComponent<ET, IPT, T> {
    final val TYPE_PARAMETER_LIST_TAG: String
        get() = "type parameter list"

    protected fun addTypeParameterListToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>) {
        addTypeParameterListToInsertPlaceMap(p, insertPlaceMap, 0)
    }

    protected fun addTypeParameterListToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ) {
        val list = p.getTypeParameterList()
        if (list != null && hasElement(list)) {
            insertPlaceMap.put(TYPE_PARAMETER_LIST_TAG, list.toSmartInsertPlace().shiftRight(delta))
        }
    }

    protected fun prepareTypeParameterListVariants(
              p: ET
            , variants: MutableMap<String, FormatSet>
            , context: VariantConstructionContext
    ) {
        val listVariants = getTypeParameterListVariants(p, context)
        if (listVariants.isEmpty()) { return }
        variants.put(TYPE_PARAMETER_LIST_TAG, listVariants)
    }

    protected fun getTypeParameterListVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val typeParameterList = p.getTypeParameterList()
        if ((typeParameterList == null) || !hasElement(typeParameterList)) { return printer.getEmptySet() }
        val variants = printer.getVariants(typeParameterList, context)
        return variants
    }

    override protected fun isTemplateSuitable(p: ET, tmplt: T): Boolean =
            isTemplateSuitable_TypeParameterListOwner(p, tmplt)

    protected fun isTemplateSuitable_TypeParameterListOwner(p: ET, tmplt: T): Boolean {
        return hasElement(p.getTypeParameterList()) == (tmplt.insertPlaceMap.get(TYPE_PARAMETER_LIST_TAG) != null)
    }
}