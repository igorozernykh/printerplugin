package org.jetbrains.likePrinter.components

import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiParameterList
import com.intellij.psi.PsiMethod
import com.intellij.psi.PsiLambdaExpression
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace

import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext
import org.jetbrains.format.emptyFormatList
import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ParameterListOwner<ET: PsiElement, IPT: SmartInsertPlace, T: Template<IPT>>: PsiElementComponent<ET, IPT, T> {
    final val PARAMETER_LIST_TAG: String
        get() = "parameter list"

    protected fun addParameterListToInsertPlaceMap(p: ET, insertPlaceMap: MutableMap<String, SmartInsertPlace>): Boolean =
            addParameterListToInsertPlaceMap(p, insertPlaceMap, 0)

    protected fun addParameterListToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val listSIP = getParameterList(p)?.toSmartInsertPlace()
        if (listSIP == null) { return false }
        insertPlaceMap.put(PARAMETER_LIST_TAG, listSIP.shiftRight(delta))
        /*
                SmartInsertPlace(
                  listSIP.range.shiftRight(delta)
                , listSIP.fillConstant
                , Box.getEverywhereSuitable()
                )
        )
        */
        return true
    }

    protected fun prepareParameterListVariants(
            p: ET, variants: MutableMap<String, FormatSet>, context: VariantConstructionContext
    ) {
        val listVariants = getParameterListVariants(p, context)
        if (listVariants.isEmpty()) { return }
        variants.put(PARAMETER_LIST_TAG, listVariants)
    }

    protected fun getParameterListVariants(p: ET, context: VariantConstructionContext): FormatSet {
        val parameterList = getParameterList(p)
        if (parameterList == null) { return printer.getEmptySet() }
        val variants = printer.getVariants(parameterList, context)
        return variants
    }

    private fun getParameterList(p: PsiElement): PsiParameterList? =
            when (p) {
                is PsiMethod           -> p.getParameterList()
                is PsiLambdaExpression -> p.getParameterList()
                else -> null
            }

}