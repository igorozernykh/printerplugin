package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiElementComponent
import com.intellij.psi.PsiElement
import org.jetbrains.likePrinter.templateBase.template.Template
import com.intellij.psi.PsiMethod
import org.jetbrains.likePrinter.util.psiElement.toSmartInsertPlace
import org.jetbrains.likePrinter.printer.CommentConnectionUtils.VariantConstructionContext

import org.jetbrains.format.FormatSet

/**
 * User: anlun
 */
public interface ReturnTypeOwner<ET: PsiMethod, T: Template<SmartInsertPlace>>: PsiElementComponent<ET, SmartInsertPlace, T> {
    final val RETURN_TYPE_TAG: String
        get() = "return type"

    protected fun addReturnTypeToInsertPlaceMap(
              p: ET
            , insertPlaceMap: MutableMap<String, SmartInsertPlace>
            , delta: Int
    ): Boolean {
        val returnTypeSIP = p.getReturnTypeElement()?.toSmartInsertPlace()
        if (returnTypeSIP == null) { return false }
        insertPlaceMap.put(RETURN_TYPE_TAG, returnTypeSIP.shiftRight(delta))
        return true
    }

    protected fun getReturnTypeVariants(p: PsiMethod, context: VariantConstructionContext): FormatSet {
        val returnType = p.getReturnTypeElement()
        if (returnType == null) { return printer.getEmptySet() }
        return printer.getVariants(returnType, context)
    }
}