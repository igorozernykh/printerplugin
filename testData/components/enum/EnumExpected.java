enum T {
  N1 {
    void foobar() { return; }
  }, N2 {
    void foobar() { return; }
  }, N3 {
    void foobar() { return; }
  }, N4("%$%^^") {
    void foobar() { return; }
  };

  abstract void foobar();
}