import org.jetbrains.format.FormatSet
import org.jetbrains.format.Format
import com.intellij.openapi.util.TextRange
import org.jetbrains.likePrinter.util.base.insertToText
import org.junit.Test
import kotlin.test.assertTrue
import org.jetbrains.likePrinter.util.base.InsertPlace

public class BaseTest {
    /*
Test fun insertFormatListsToText_EmptyText() {
    val text = ""

    val maxWidth = 100
    val fmtList1   = FormatSet.toSet(maxWidth, Format.line("Hello"))
    val firstElem  = Pair(InsertPlace(TextRange(1,   5), InsertPlace.DOESNT_START_WITH_NEW_LINE), fmtList1)

    val fmtList2   = FormatSet.toSet(maxWidth, Format.line("Goodbye"))
    val secondElem = Pair(InsertPlace(TextRange(6, 177), InsertPlace.STARTS_WITH_NEW_LINE      ), fmtList2)
    val fmtListsWithRanges = listOf(firstElem, secondElem)

    val func = { () -> FormatSet.insertToText(maxWidth, text, fmtListsWithRanges) }
    failsWith(IndexOutOfBoundsException().javaClass, func)
}
*/

    private fun insertFormatListsToTextTest(secondInsertFillConstant: Int, expectedResult: String): Boolean {
        val text = "a b"

        val firstText  = "Hello"
        val secondText = "Goodbye\ntest!"

        val maxWidth = 100
        val fmtList1  = FormatSet.initial(maxWidth, Format.text(firstText))
        val firstElem = Pair(InsertPlace(TextRange(0, 1), InsertPlace.DOESNT_START_WITH_NEW_LINE), fmtList1)

        val fmtList2   = FormatSet.initial(maxWidth, Format.text(secondText))
        val secondElem = Pair(InsertPlace(TextRange(2, 3), secondInsertFillConstant), fmtList2)
        val fmtListsWithRanges = listOf(firstElem, secondElem)

        val resultList = insertToText(maxWidth, text, fmtListsWithRanges)
        return resultList.head().toString().equals(expectedResult)
    }

    Test fun insertFormatListsToText_BesideAddition() {
        val expectedResult =
                "Hello Goodbye\n" +
                        "      test!"
        assertTrue(insertFormatListsToTextTest(InsertPlace.STARTS_WITH_NEW_LINE, expectedResult), "Incorrect insert to text.")
    }

    Test fun insertFormatListsToTextTest_FillAddition() {
        val expectedResult =
                "Hello Goodbye\n" +
                        "test!"
        assertTrue(insertFormatListsToTextTest(InsertPlace.DOESNT_START_WITH_NEW_LINE, expectedResult), "Incorrect insert to text.")
    }

    Test fun insertFormatListsToTextTest_2FillAddition() {
        val expectedResult =
                "Hello Goodbye\n" +
                        "  test!"
        val fillConstant = 2
        assertTrue(insertFormatListsToTextTest(fillConstant, expectedResult), "Incorrect insert to text.")
    }

}