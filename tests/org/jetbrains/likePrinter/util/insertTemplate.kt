package org.jetbrains.likePrinter.util.insertTemplate

import org.jetbrains.likePrinter.util.base.InsertPlace
import org.junit.Test
import com.intellij.openapi.util.TextRange
import org.jetbrains.format.toFormatList
import kotlin.test.assertEquals
import org.jetbrains.format.FormatSet
import org.jetbrains.format.FormatList
import org.jetbrains.format.toFormat
import org.jetbrains.likePrinter.templateBase.template.ListInsertTemplate
import org.jetbrains.likePrinter.templateBase.template.insertPlaceListTemplateToFormatList

/**
 * User: anlun
 */

class ListInsertTemplateTest {
    Test fun insertPlaceListTemplateToFormatList_Test() {
        val textProducer: (String, String, String) -> String = { a, b, c ->
            "implements $a, $b, $c"
        }
        val implementsListText = textProducer("A", "B", "C")

        val aPos   = implementsListText.indexOf("A")
        val aRange = TextRange(aPos, aPos + 1)
        val aInsertPlace = InsertPlace(aRange, InsertPlace.DOESNT_START_WITH_NEW_LINE)

        val bPos   = implementsListText.indexOf("B")
        val bRange = TextRange(bPos, bPos + 1)
        val bInsertPlace = InsertPlace(bRange, InsertPlace.DOESNT_START_WITH_NEW_LINE)

        val cPos   = implementsListText.indexOf("C")
        val cRange = TextRange(cPos, cPos + 1)
        val cInsertPlace = InsertPlace(cRange, InsertPlace.DOESNT_START_WITH_NEW_LINE)

        val implementsListTmplt = ListInsertTemplate(
                implementsListText, listOf(aInsertPlace, bInsertPlace, cInsertPlace)
        )

        val maxWidth = 100
        val elemVariants = listOf(
                FormatSet.initial(maxWidth, "QWE"), FormatSet.initial(maxWidth, "ASD"), FormatSet.initial(maxWidth, "FOOBAR")
        )
        val expected = textProducer("QWE", "ASD", "FOOBAR")

        val formatSet = insertPlaceListTemplateToFormatList(100, implementsListTmplt, elemVariants)
        val head = formatSet.head()
        val text = if (head != null) head.toText(0, "") else ""
        assertEquals(expected, text)
    }
}
