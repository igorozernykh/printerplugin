package org.jetbrains.likePrinter.components

import org.junit.Test
import com.intellij.testFramework.fixtures.LightCodeInsightFixtureTestCase
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.readFile
import kotlin.test.assertEquals
import org.jetbrains.likePrinter.util.string.lineEndTrim
import com.intellij.psi.PsiJavaFile

/**
 * User: anlun
 */
public class MethodTest: LightCodeInsightFixtureTestCase() {

    Test fun testMethodTemplate_1() {
        val methodTemplateFileName = PATH_TO_COMPONENTS + "MethodT.java"
        val templateFileContent = readFile(methodTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val methodToReprintFileName = PATH_TO_COMPONENTS + "MethodEx.java"
        val toReprintFileContent = readFile(methodToReprintFileName).trim()
        val methodToReprint = factory.createMethodFromText(toReprintFileContent, null)

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, MAX_WIDTH)
        val variants = printer.getVariants(methodToReprint)

//        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        val expectedResultFileName = PATH_TO_COMPONENTS + "MethodExpected.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text.trim(), "Incorrect result!")
    }

    // Because of comment doubling bug
    Test fun testMethodTemplate_Comment() {
        val methodTemplateFileName = PATH_TO_COMPONENTS + "MethodT.java"
        val templateFileContent = readFile(methodTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val methodToReprintFileName = PATH_TO_COMPONENTS + "MethodEx_withComments.java"
        val toReprintFileContent = readFile(methodToReprintFileName)
        val methodToReprint = factory.createMethodFromText(toReprintFileContent, null)

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, MAX_WIDTH)
        val variants = printer.getVariants(methodToReprint)

//        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        val expectedResultFileName = PATH_TO_COMPONENTS + "MethodExpected_withComments.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text.trim(), "Incorrect result!")
    }

    Test fun testMethodTemplate_ParameterList_Width_40() {
        val methodTemplateFileName = PATH_TO_COMPONENTS + "MethodT.java"
        val templateFileContent = readFile(methodTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val methodToReprintFileName = PATH_TO_COMPONENTS + "MethodEx_ParameterList.java"
        val toReprintFileContent = readFile(methodToReprintFileName)
        val methodToReprint = factory.createMethodFromText(toReprintFileContent, null)

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, 40)
        val variants = printer.getVariants(methodToReprint)

//        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        val expectedResultFileName = PATH_TO_COMPONENTS + "MethodExpected_ParameterList.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text.trim(), "Incorrect result!")
    }

    Test fun testMethodTemplate_ParameterList_Width_20() {
        val methodTemplateFileName = PATH_TO_COMPONENTS + "MethodT.java"
        val templateFileContent = readFile(methodTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val methodToReprintFileName = PATH_TO_COMPONENTS + "MethodEx_ParameterList.java"
        val toReprintFileContent = readFile(methodToReprintFileName)
        val methodToReprint = factory.createMethodFromText(toReprintFileContent, null)

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, 20)
        val variants = printer.getVariants(methodToReprint)

//        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        val expectedResultFileName = PATH_TO_COMPONENTS + "MethodExpected_20_ParameterList.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text.trim().lineEndTrim(), "Incorrect result!")
    }

    Test fun testMethodTemplate_ParameterList_Width_60() {
        val methodTemplateFileName = PATH_TO_COMPONENTS + "MethodT_2.java"
        val templateFileContent = readFile(methodTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val methodToReprintFileName = PATH_TO_COMPONENTS + "ClassEx_withComments_2.java"
        val toReprintFileContent = readFile(methodToReprintFileName)
        val dummyClass = factory.createClassFromText(toReprintFileContent, null)
        val classToReprint = dummyClass.getInnerClasses()[0]

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, MAX_WIDTH)
        val variants = printer.getVariants(classToReprint)

        //        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "").lineEndTrim() else ""

        val expectedResultFileName = PATH_TO_COMPONENTS + "ClassExpected_withComments_2.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text.trim(), "Incorrect result!")
    }

    Test fun testMethodInClassTemplate_Comment() {
        val methodTemplateFileName = PATH_TO_COMPONENTS + "MethodT.java"
        val templateFileContent = readFile(methodTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val methodToReprintFileName = PATH_TO_COMPONENTS + "ClassEx_withComments.java"
        val toReprintFileContent = readFile(methodToReprintFileName)
        val dummyClass = factory.createClassFromText(toReprintFileContent, null)
        val classToReprint = dummyClass.getInnerClasses()[0]

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, MAX_WIDTH)
        val variants = printer.getVariants(classToReprint)

//        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        val expectedResultFileName = PATH_TO_COMPONENTS + "ClassExpected_withComments.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text.trim(), "Incorrect result!")
    }

}