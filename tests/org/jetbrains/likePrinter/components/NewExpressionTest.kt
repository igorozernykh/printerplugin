package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.readFile
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.string.lineEndTrim
import org.junit.Test
import kotlin.test.assertEquals
import com.intellij.psi.PsiJavaFile


/**
 * User: anlun
 */
public class NewExpressionTest: ComponentTest() {
    override fun getOurPathToComponents(): String = PATH_TO_COMPONENTS

    fun getTestNewExpression(
               templateFileName: String
            , toReprintFileName: String
    ): String {
        val templateFileName = PATH_TO_COMPONENTS + templateFileName
        val templateFileContent = readFile(templateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val toReprintFileName = PATH_TO_COMPONENTS + toReprintFileName
        val toReprintFileContent = readFile(toReprintFileName).trim()
        val expressionToReprint =
                factory.createExpressionFromText(toReprintFileContent, null)

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, MAX_WIDTH)
        val variants = printer.getVariants(expressionToReprint)

        //        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        return text.lineEndTrim()
    }

    Test fun testNewExpression_1() {
        val text = getTestNewExpression("NewExpressionT_1.java", "NewExpressionEx_1.java")
        val expectedResultFileName = PATH_TO_COMPONENTS + "NewExpressionExpected_1.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }

    Test fun testNewExpression_2() {
        val text = getTestNewExpression("NewExpressionT_2.java", "NewExpressionEx_2.java")
        val expectedResultFileName = PATH_TO_COMPONENTS + "NewExpressionExpected_2.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }

    Test fun testNewExpression_3() {
        val text = getTestNewExpression("NewExpressionT_2.java", "NewExpressionEx_3.java")
        val expectedResultFileName = PATH_TO_COMPONENTS + "NewExpressionExpected_3.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }

    Test fun testNewExpression_Anonymous() {
        val text = getTestNewExpression("NewExpressionT_Anonymous.java", "NewExpressionEx_Anonymous.java")
        val expectedResultFileName = PATH_TO_COMPONENTS + "NewExpressionExpected_Anonymous.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }

}