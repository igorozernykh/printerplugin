package org.jetbrains.likePrinter.components

import org.junit.Test
import com.intellij.testFramework.fixtures.LightCodeInsightFixtureTestCase
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.readFile
import kotlin.test.assertEquals
import org.jetbrains.likePrinter.util.string.lineEndTrim
import com.intellij.psi.PsiDeclarationStatement
import com.intellij.psi.PsiVariable
import org.jetbrains.likePrinter.components.variables.getBracketCount
import org.jetbrains.likePrinter.components.variables.VariableComponent
import org.jetbrains.likePrinter.templateBase.template.SmartInsertPlace
import org.jetbrains.likePrinter.templateBase.template.PsiTemplateGen
import com.intellij.openapi.util.TextRange
import com.intellij.psi.PsiJavaFile

/**
 * User: anlun
 */
public class VariableTest: ComponentTest() {
    override fun getOurPathToComponents(): String = PATH_TO_COMPONENTS

    class PublicVariableComponent(printer: Printer): VariableComponent(printer) {
        public fun getTemplateFromElement_public(
                newP: PsiVariable
        ): PsiTemplateGen<PsiVariable, SmartInsertPlace>? = getTemplateFromElement(newP)

        public fun getArrayBracketTag(): String = ARRAY_BRACKET_TAG
    }

    fun variableBracketList(variableText: String): Int {
        val variable = getVariableFromText(variableText)
        val bracketCount = variable.getBracketCount()
        return bracketCount
    }

    Test fun testVariableBracketList_empty() {
        val variableText = "Int a = 1;"
        assertEquals(0, variableBracketList(variableText))
    }
    Test fun testVariableBracketList_1() {
        val variableText = "Int a[] = {1};"
        assertEquals(1, variableBracketList(variableText))
    }
    Test fun testVariableBracketList_2() {
        val variableText = "Int a[][] = {{1}};"
        assertEquals(2, variableBracketList(variableText))
    }

    fun getBracketRangeInTemplate(variableText: String): TextRange {
        val variable = getVariableFromText(variableText)
        val project = getProject()
        if (project == null) { throw NullPointerException() }
        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText("class A{}", null)
        val javaFile = tmpltPsiClass.getContainingFile() as PsiJavaFile

        val printer = Printer.create(javaFile, project, MAX_WIDTH)
        val variableComponent = PublicVariableComponent(printer)
        val variableTmplt = variableComponent.getTemplateFromElement_public(variable)
        val arrayBracketSIP = variableTmplt!!.insertPlaceMap.get(variableComponent.getArrayBracketTag())
        val arrayBracketRange = arrayBracketSIP!!.range
        return arrayBracketRange
    }

    Test fun testVariableBracketListTmplt_1() {
        val variableText = "Int a[] = {1};"

        // actual
        val arrayBracketRange = getBracketRangeInTemplate(variableText)

        // expected
        val startOffset = variableText.indexOf('[')
        val   endOffset = variableText.lastIndexOf(']') + 1

        assertEquals(TextRange(startOffset, endOffset), arrayBracketRange)

    }

    Test fun testVariableBracketListTmplt_2() {
        val variableText = "Int a[][] = {{1}};"

        // actual
        val arrayBracketRange = getBracketRangeInTemplate(variableText)

        // expected
        val startOffset = variableText.indexOf('[')
        val   endOffset = variableText.lastIndexOf(']') + 1

        assertEquals(TextRange(startOffset, endOffset), arrayBracketRange)

    }

    Test fun testVariableTemplate_1() {
        val variableTemplateFileName = PATH_TO_COMPONENTS + "VariableT.java"
        val templateFileContent = readFile(variableTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val variableToReprintFileName = PATH_TO_COMPONENTS + "VariableEx.java"
        val toReprintFileContent = readFile(variableToReprintFileName).trim()
        val classToReprint =
                factory.createClassFromText(toReprintFileContent, null)
                       .getAllInnerClasses()[0]

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, MAX_WIDTH + 2)
        val variants = printer.getVariants(classToReprint)

        //        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        val expectedResultFileName = PATH_TO_COMPONENTS + "VariableExpected.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text.trim(), "Incorrect result!")
    }
}