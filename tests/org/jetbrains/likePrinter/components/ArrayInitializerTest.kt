package org.jetbrains.likePrinter.components

import com.intellij.testFramework.fixtures.LightCodeInsightFixtureTestCase
import org.junit.Test
import org.jetbrains.likePrinter.readFile
import kotlin.test.assertEquals
import org.jetbrains.likePrinter.util.string.lineEndTrim
import org.jetbrains.likePrinter.printer.Printer
import com.intellij.psi.PsiJavaFile

/**
 * User: anlun
 */
public class ArrayInitializerTest: ComponentTest() {
    override fun getOurPathToComponents(): String = PATH_TO_COMPONENTS

    fun getReprintedArrayInitializerText(widthToSuit: Int): String {
        val arrayInitTemplateFileName = PATH_TO_COMPONENTS + "ArrayInitializerT.java"
        val templateFileContent = readFile(arrayInitTemplateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val arrayInitToReprintFileName = PATH_TO_COMPONENTS + "ArrayInitializerEx.java"
        val toReprintFileContent = readFile(arrayInitToReprintFileName).trim()
        val variableDeclarationToReprint =
                factory.createStatementFromText(toReprintFileContent, null)

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, widthToSuit)
        val variants = printer.getVariants(variableDeclarationToReprint)

        //        assertEquals(1, variants.size(), "More then one variant!")
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        return text.trim()
    }

    Test fun testVariable_25() {
        val resultText = getReprintedArrayInitializerText(25 + 1)
        val expectedResultFileName = PATH_TO_COMPONENTS + "ArrayInitializerExepcted_25.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), resultText.lineEndTrim(), "Incorrect result!")
    }

    Test fun testVariable_20() {
        val resultText = getReprintedArrayInitializerText(20 + 1)
        val expectedResultFileName = PATH_TO_COMPONENTS + "ArrayInitializerExepcted_20.java"
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), resultText.lineEndTrim(), "Incorrect result!")
    }
}