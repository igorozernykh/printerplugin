package org.jetbrains.likePrinter.components

import org.jetbrains.likePrinter.readFile
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.string.lineEndTrim
import org.junit.Test
import kotlin.test.assertEquals
import com.intellij.psi.PsiJavaFile
import com.intellij.psi.PsiElement

/**
 * User: anlun
 */
public class TypeElementTest: ComponentTest() {
    final val OUR_PATH_TO_COMPONENTS: String
        get() = PATH_TO_COMPONENTS + "types/"

    override fun getOurPathToComponents(): String = OUR_PATH_TO_COMPONENTS

    fun getTestTypeElement(
               templateFileName: String
            ,  isParameterTmplt: Boolean
            , toReprintFileName: String
    ): String {
        val templateFileContent = readFile(OUR_PATH_TO_COMPONENTS + templateFileName)

        val factory = getElementFactory()
        val tmpltPsiClass = factory!!.createClassFromText(templateFileContent, null)

        val project = getProject()!!
        val toReprintFileContent = readFile(OUR_PATH_TO_COMPONENTS + toReprintFileName).trim()

        val psiElementToReprint: PsiElement =
            if (isParameterTmplt) factory.createParameterFromText(toReprintFileContent, null)
            else factory.createClassFromText(toReprintFileContent, null).getAllInnerClasses()[0]

        val printer = Printer.create(tmpltPsiClass.getContainingFile() as PsiJavaFile, project, MAX_WIDTH)
        val variants = printer.getVariants(psiElementToReprint)
        val format = variants.head()
        return if (format != null) format.toText(0, "").lineEndTrim() else ""
    }

    fun testTypeElement(
              tmpltFilePath: String, isParameterTmplt: Boolean
        , toReprintFilePath: String, expectedFilePath: String
    ) {
        val text = getTestTypeElement(tmpltFilePath, isParameterTmplt, toReprintFilePath)
        val expectedResultFileName = OUR_PATH_TO_COMPONENTS + expectedFilePath
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }

    Test fun testTypeElement_1() {
        testTypeElement("TypeElementT_1.java", true, "TypeElementEx_1.java", "TypeElementExpected_1.java")
    }

    Test fun testTypeElement_2() {
        testTypeElement("TypeElementT_1.java", true, "TypeElementEx_2.java", "TypeElementExpected_2.java")
    }

    Test fun testTypeElement_3() {
        testTypeElement("TypeElementT_3.java", false, "TypeElementEx_3.java", "TypeElementExpected_3.java")
    }

    Test fun testTypeElement_4() {
        testTypeElement("TypeElementT_4.java", false, "TypeElementEx_4.java", "TypeElementExpected_4.java")
    }

    Test fun testTypeElement_5() {
        testTypeElement("TypeElementT_1.java", true, "TypeElementEx_5.java", "TypeElementExpected_5.java")
    }

    Test fun testTypeElement_6() {
        testTypeElement("TypeElementT_1.java", true, "TypeElementEx_6.java", "TypeElementExpected_6.java")
    }

    Test fun testTypeElement_7() {
        testTypeElement("TypeElementT_1.java", true, "TypeElementEx_7.java", "TypeElementExpected_7.java")
    }
}