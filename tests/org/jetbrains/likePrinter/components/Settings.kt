package org.jetbrains.likePrinter.components

import com.intellij.testFramework.fixtures.LightCodeInsightFixtureTestCase
import com.intellij.psi.PsiVariable
import com.intellij.psi.PsiDeclarationStatement
import org.jetbrains.likePrinter.readFile
import com.intellij.psi.PsiFileFactory
import com.intellij.psi.PsiJavaFile
import org.jetbrains.likePrinter.printer.Printer
import org.jetbrains.likePrinter.util.string.lineEndTrim
import kotlin.test.assertEquals

/**
 * User: anlun
 */
val PATH_TO_COMPONENTS: String = "components/"
val MAX_WIDTH: Int = 40

abstract class ComponentTest(): LightCodeInsightFixtureTestCase() {
    abstract fun getOurPathToComponents(): String

    open fun getOurMaxWidth(): Int = MAX_WIDTH

    fun getTest(
               templateFileName: String
            , toReprintFileName: String
    ): String {
        val templateFileName = getOurPathToComponents() + templateFileName
        val templateFileContent = readFile(templateFileName)

        val project = getProject()
        if (project == null) { throw NullPointerException() }

        val tmpltPsiFile =
                PsiFileFactory.getInstance(project)!!.createFileFromText(templateFileName, templateFileContent)
                as PsiJavaFile

        val toReprintFileName = getOurPathToComponents() + toReprintFileName
        val toReprintFileContent = readFile(toReprintFileName).trim()
        val toReprintPsiFile =
                PsiFileFactory.getInstance(project)!!.createFileFromText(toReprintFileName, toReprintFileContent)
                as PsiJavaFile

        val printer = Printer.create(tmpltPsiFile, project, getOurMaxWidth())
        val variants = printer.getVariants(toReprintPsiFile)
        val format = variants.head()
        val text = if (format != null) format.toText(0, "") else ""

        return text.lineEndTrim()
    }

    fun test(testTemplateFileName: String, testExampleFileName: String, expectedResultFileName: String) {
        val text = getTest(testTemplateFileName, testExampleFileName)
        val expectedResultFileName = getOurPathToComponents() + expectedResultFileName
        val expectedResult = readFile(expectedResultFileName)
        assertEquals(expectedResult.trim(), text, "Incorrect result!")
    }


    fun getVariableFromText(variableText: String): PsiVariable {
        val factory = getElementFactory()
        val variableDeclarationStatement =
                factory!!.createStatementFromText(variableText, null) as? PsiDeclarationStatement
        val variable =
                variableDeclarationStatement!!.getDeclaredElements()[0] as PsiVariable
        return variable
    }
}
